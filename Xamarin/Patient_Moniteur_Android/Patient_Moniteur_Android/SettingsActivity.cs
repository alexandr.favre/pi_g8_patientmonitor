﻿
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;

namespace Patient_Moniteur_Android
{
    [Activity(Label = "Paramètres", Theme = "@style/AppTheme")]
    public class SettingsActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.parameters);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            EditText serverIP = FindViewById<EditText>(Resource.Id.server_ip);

            //récupération de l'adresse IP sauvegardé
            string savedServerIP = MyModelAndroid.ModelInstance.GetServerIP();

            //affichage de l'adresse IP sauvegardé si elle existe
            if (savedServerIP != null) serverIP.Append(savedServerIP);

            // ajout d'un event listener sur le boutton "Confirmer"
            FindViewById<Button>(Resource.Id.confirm_ip_server).Click += (o, e) =>
            {
                MyModelAndroid.ModelInstance.SaveServerIP(serverIP.Text);// Sauvegarde de l'IP
                Finish();// Fermeture de la vue
            };
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            Finish();
            return true;
        }
    }
}