﻿using System;
using Patient_Moniteur;
using System.IO;

namespace Patient_Moniteur_Android
{
    class MyModelAndroid : MyModel
    {
        private static MyModelAndroid _ModelInstance;

        private static string Connection
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), SQLiteHelper.DATABASENAME);
            }
        }

        public static MyModelAndroid ModelInstance
        {
            get
            {
                if (_ModelInstance == null)
                {
                    _ModelInstance = new MyModelAndroid();
                }
                return _ModelInstance;
            }
        }

        private MyModelAndroid() : base(Connection)
        {

        }
    }
}