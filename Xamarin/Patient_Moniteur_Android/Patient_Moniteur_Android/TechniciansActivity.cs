﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Patient_Moniteur;

namespace Patient_Moniteur_Android
{
    [Activity(Label = "Techniciens")]
    public class TechniciansActivity : AppCompatActivity
    {
        private TechniciansListViewAdapter tva;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.technicians);
        }

        protected async override void OnResume()
        {
            base.OnResume();

            // Get Technicians
            List<Technician> tech_list = await MyModelAndroid.ModelInstance.GetAllTechnicians();

            ListView technicians = FindViewById<ListView>(Resource.Id.technicians_list);
            if (tva == null)
            {
                tva = new TechniciansListViewAdapter(tech_list, this);
                technicians.ItemClick += (sender, e) =>
                {
                    var tech_info_activity = new Intent(this, typeof(TechnicianInfoActivity));
                    var b = new Bundle();
                    b.PutString("technician", JsonConvert.SerializeObject(tva[e.Position]));
                    tech_info_activity.PutExtras(b);
                    StartActivity(tech_info_activity);
                };
            }
            else tva.SetTechList(tech_list);

            technicians.Adapter = tva;
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            this.MenuInflater.Inflate(Resource.Menu.menu_deco_resp_tech, menu);

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_logout_resp_tech:
                    var param_activity = new Intent(this, typeof(LoginActivity));
                    MyModelAndroid.ModelInstance.ResetAccount();
                    StartActivity(param_activity);
                    Finish();
                    break;
            }

            return true;
        }

    }

    class TechniciansListViewAdapter : BaseAdapter<Technician>
    {

        private List<Technician> tech_list;
        private TechniciansActivity context;

        public TechniciansListViewAdapter(List<Technician> visits, TechniciansActivity context)
        {
            this.tech_list = visits;
            this.context = context;
        }

        public void SetTechList(List<Technician> visits)
        {
            this.tech_list = visits;
        }

        public override Technician this[int position] => tech_list[position];

        public override int Count => tech_list.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            Technician tech = tech_list[position];

            View view = convertView;
            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.patient_date_cell, null);
            }

            view.FindViewById<TextView>(Resource.Id.pdc_name_forname).Text = tech.Prenom + " " + tech.Nom;
            view.FindViewById<TextView>(Resource.Id.pdc_date).Text = "";

            return view;
        }
    }
}