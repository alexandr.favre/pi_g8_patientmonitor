﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Android.Views;
using Android.Content;
using AlertDialog = Android.App.AlertDialog;

namespace Patient_Moniteur_Android
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class LoginActivity : AppCompatActivity
    {

        // Docker : 10.0.2.2 | Toolbox : 192.168.99.100

        protected override void OnCreate(Bundle savedInstanceState)
        {
            Window.SetSoftInputMode(SoftInput.AdjustPan);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            LoginRedirection(MyModelAndroid.ModelInstance.GetIdentifierValue());

            EditText emailTxt = FindViewById<EditText>(Resource.Id.emailTxt);
            EditText passwordTxt = FindViewById<EditText>(Resource.Id.passwordTxt);

            string savedEmail = MyModelAndroid.ModelInstance.GetEmailValue();
            if (savedEmail != null) emailTxt.Append(savedEmail);

            FindViewById<Button>(Resource.Id.btn_connexion).Click += async (o, e) =>
            {
                emailTxt.Enabled = false;
                passwordTxt.Enabled = false;
                string identifierValue = await MyModelAndroid.ModelInstance.Login(emailTxt.Text, passwordTxt.Text);
                emailTxt.Enabled = true;
                passwordTxt.Enabled = true;
                LoginRedirection(identifierValue);
                if (identifierValue == "0") ShowAlert("Erreur login", "Identifiants incorrects");
                else if (identifierValue == null) ShowAlert("Erreur login", "Serveur inaccessible");
            };
        }

        private async void LoginRedirection(string identifierValue)
        {
            if (identifierValue == "1")
            {
                var intent = new Intent(this, typeof(PlanningActivity));
                StartActivity(intent);
            }
            else if (identifierValue == "2")
            {
                if (await MyModelAndroid.ModelInstance.IsLogged())
                {
                    var intent = new Intent(this, typeof(TechniciansActivity));
                    StartActivity(intent);
                }
            }
        }

        private void ShowAlert(string title, string message)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.SetTitle(title);
            alertDialog.SetMessage(message);
            alertDialog.SetNeutralButton("OK", delegate
            {
                alertDialog.Dispose();
            });
            alertDialog.Show();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            this.MenuInflater.Inflate(Resource.Menu.menu_main, menu);

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            var param_activity = new Intent(this, typeof(SettingsActivity));
            StartActivity(param_activity);
            return true;
        }
    }
}