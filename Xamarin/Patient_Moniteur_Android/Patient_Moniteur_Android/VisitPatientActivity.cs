﻿
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Patient_Moniteur;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Patient_Moniteur_Android
{
    [Activity(Label = "", Theme = "@style/AppTheme")]
    public class VisitPatientActivity : AppCompatActivity
    {
        private Visit visit;

        private List<VisitPicture> pictures = new List<VisitPicture>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.visit_patient);

            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            EditText lastName = FindViewById<EditText>(Resource.Id.last_name);
            EditText firstName = FindViewById<EditText>(Resource.Id.first_name);
            EditText dateVisit = FindViewById<EditText>(Resource.Id.date_visit);
            EditText address = FindViewById<EditText>(Resource.Id.address);
            EditText city = FindViewById<EditText>(Resource.Id.city);
            EditText phone = FindViewById<EditText>(Resource.Id.phone);
            Spinner device = FindViewById<Spinner>(Resource.Id.device);
            Spinner doctor = FindViewById<Spinner>(Resource.Id.doctor);
            Button confirmBtn = FindViewById<Button>(Resource.Id.confirmer_btn);
            Button addPictureBtn = FindViewById<Button>(Resource.Id.add_picture_btn);
            TextView nbPictures = FindViewById<TextView>(Resource.Id.nb_picture);

            visit = JsonConvert.DeserializeObject<Visit>(Intent.Extras.GetString("visit"));
            Patient patient = MyModelAndroid.ModelInstance.GetPatientOffline(visit.Patient);

            var addressJson = JObject.Parse(patient.Adresse);
            lastName.Append(patient.Nom);
            firstName.Append(patient.Prenom);
            dateVisit.Append(visit.Date);
            address.Append(addressJson["adresse"].ToString());
            city.Append(addressJson["ville"].ToString());
            phone.Append(patient.NumTel);

            List<Doctor> doctors = MyModelAndroid.ModelInstance.GetAllDoctorsOffline();
            var doctorAdapter = new ArrayAdapter<Doctor>(this, Android.Resource.Layout.SimpleSpinnerItem, doctors);
            doctor.Adapter = doctorAdapter;
            int selectedDoctorIndex = doctors.FindIndex(i => i.IDMedecin == patient.Medecin);
            doctor.SetSelection(doctorAdapter.GetPosition(selectedDoctorIndex));

            List<Device> devices = MyModelAndroid.ModelInstance.GetAllDevicesOffline();
            var deviceAdapter = new ArrayAdapter<Device>(this, Android.Resource.Layout.SimpleSpinnerItem, devices);
            device.Adapter = deviceAdapter;
            int selectedDeviceIndex = devices.FindIndex(i => i.NumAppareil == patient.Appareil);
            device.SetSelection(selectedDeviceIndex);

            SupportActionBar.Title = patient.Prenom + " " + patient.Nom;

            confirmBtn.Click += (o, e) =>
            {
                Patient updatedPatient = new Patient(patient.IDPatient,
                                                     doctors[doctor.SelectedItemPosition].IDMedecin,
                                                     devices[device.SelectedItemPosition].NumAppareil,
                                                     lastName.Text,
                                                     firstName.Text,
                                                     "{ 'adresse': '" + address.Text + "', 'ville': '" + city.Text + "' }",
                                                     phone.Text,
                                                     patient.Timestamp);

                if (patient.Nom != updatedPatient.Nom ||
                    patient.Prenom != updatedPatient.Prenom ||
                    patient.Medecin != updatedPatient.Medecin ||
                    patient.Appareil != updatedPatient.Appareil ||
                    patient.Adresse != updatedPatient.Adresse ||
                    patient.NumTel != updatedPatient.NumTel)
                {
                    MyModelAndroid.ModelInstance.UpdatePatientOffline(updatedPatient);
                }
                MyModelAndroid.ModelInstance.UpdateVisitOffline(new Visit(visit.Id, visit.Technicien, visit.Patient, visit.Date, true, visit.Timestamp));
                if (pictures.Count > 0) MyModelAndroid.ModelInstance.AddVisitPicturesOffline(pictures);
                Finish();
            };

            addPictureBtn.Click += async (o, e) =>
            {
                Stream sig = await PicturePicker();
                using (var memoryStream = new MemoryStream())
                {
                    sig.CopyTo(memoryStream);
                    sig.Dispose();
                    pictures.Add(new VisitPicture(visit.Patient, DateTime.Today.ToString("yyyy-MM-dd"), memoryStream.ToArray()));
                    if (pictures.Count > 0) nbPictures.Text = "[" + pictures.Count + " image(s) sélectionnée(s)]";
                }
            };
        }

        public static readonly int PickImageId = 1000;

        public TaskCompletionSource<Stream> PickImageTaskCompletionSource { set; get; }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent intent)
        {
            base.OnActivityResult(requestCode, resultCode, intent);

            if (requestCode == PickImageId)
            {
                if ((resultCode == Result.Ok) && (intent != null))
                {
                    Android.Net.Uri uri = intent.Data;
                    Stream stream = ContentResolver.OpenInputStream(uri);

                    // Set the Stream as the completion of the Task
                    PickImageTaskCompletionSource.SetResult(stream);
                }
                else
                {
                    PickImageTaskCompletionSource.SetResult(null);
                }
            }
        }

        public Task<Stream> PicturePicker()
        {
            // Define the Intent for getting images
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction(Intent.ActionGetContent);

            // Start the picture-picker activity (resumes in MainActivity.cs)
            this.StartActivityForResult(
                Intent.CreateChooser(intent, "Select Picture"),
                VisitPatientActivity.PickImageId);

            // Save the TaskCompletionSource object as a MainActivity property
            this.PickImageTaskCompletionSource = new TaskCompletionSource<Stream>();

            // Return Task object
            return this.PickImageTaskCompletionSource.Task;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}