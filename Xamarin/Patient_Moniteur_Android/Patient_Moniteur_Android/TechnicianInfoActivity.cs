﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Patient_Moniteur;
using AlertDialog = Android.App.AlertDialog;

namespace Patient_Moniteur_Android
{
    [Activity(Label = "TechnicianInfoActivity")]
    public class TechnicianInfoActivity : AppCompatActivity
    {

        private Technician technician;

        protected async override void OnCreate(Bundle savedInstanceState)
        {
            Window.SetSoftInputMode(SoftInput.AdjustPan);
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.technician_info);

            technician = JsonConvert.DeserializeObject<Technician>(Intent.Extras.GetString("technician"));


            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            EditText lastName = FindViewById<EditText>(Resource.Id.tech_last_name);
            EditText firstName = FindViewById<EditText>(Resource.Id.tech_first_name);
            EditText address = FindViewById<EditText>(Resource.Id.tech_address);
            EditText town = FindViewById<EditText>(Resource.Id.tech_town);
            EditText phone = FindViewById<EditText>(Resource.Id.tech_num);
            Button confirm = FindViewById<Button>(Resource.Id.validate_tech_mod);
            FloatingActionButton add_visit = FindViewById<FloatingActionButton>(Resource.Id.add_visit_fab);
            lastName.ClearFocus();

            //Fill EditText
            var addressJson = JObject.Parse(technician.Adresse);
            lastName.Text = technician.Nom;
            firstName.Text = technician.Prenom;
            address.Text = addressJson["adresse"].ToString();
            town.Text = addressJson["ville"].ToString();
            phone.Text = technician.NumTel;
            SupportActionBar.Title = technician.Prenom + " " + technician.Nom;

            //Confirm modification
            confirm.Click += async (sender, e) =>
            {
                Technician updatedTechnician =
                  new Technician(technician.IDTechnicien,
                                 lastName.Text,
                                 firstName.Text,
                                 "{ 'adresse': '" + address.Text + "', 'ville': '" + town.Text + "' }",
                                 phone.Text);

                bool result = await MyModelAndroid.ModelInstance.UpdateTechncian(updatedTechnician);

                if (result)
                {
                    ShowAlert("Mise à jour du technicien", "Les informations du technicien ont correctement été mises à jour");
                    SupportActionBar.Title = updatedTechnician.Prenom + " " + updatedTechnician.Nom;
                }
                else ShowAlert("Erreur mise à jour du technicien", "Serveur inaccessible");
            };

            //FAB ADD VISIT
            add_visit.Click += (sender, e) =>
             {
                 var tech_info_activity = new Intent(this, typeof(DetailsVisitActivity));
                 var b = new Bundle();
                 b.PutString("visit", null);
                 b.PutString("idtech", technician.IDTechnicien);
                 tech_info_activity.PutExtras(b);
                 StartActivity(tech_info_activity);
             };
        }


        protected async override void OnResume()
        {
            base.OnResume();
            //List Visit
            ListView visits_list = FindViewById<ListView>(Resource.Id.visits_list);
            // Get technician's planning
            string planningJson = await MyModelAndroid.ModelInstance.GetPlanningOfTechnician(technician.IDTechnicien);

            if (planningJson == null) ShowAlert("Erreur récupération du planning", "Serveur inaccessible");
            else
            {
                // Convert json to lists
                var root = JObject.Parse(planningJson);
                List<Visit> visits = root["visiter"].ToObject<List<Visit>>();
                List<Patient> patients = root["patient"].ToObject<List<Patient>>();

                VisitsListViewAdapter vva = new VisitsListViewAdapter(visits, patients, this);
                visits_list.Adapter = vva;
            }
        }

        public void ShowAlert(string title, string message)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.SetTitle(title);
            alertDialog.SetMessage(message);
            alertDialog.SetNeutralButton("OK", delegate {
                alertDialog.Dispose();
            });
            alertDialog.Show();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }




    class VisitsListViewAdapter : BaseAdapter<Visit>
    {

        private List<Visit> visit_list;
        private List<Patient> patient_visit;
        private TechnicianInfoActivity context;

        public VisitsListViewAdapter(List<Visit> visits, List<Patient> patients, TechnicianInfoActivity context)
        {
            this.visit_list = visits;
            this.patient_visit = patients;
            this.context = context;
        }

        public void SetVisitList(List<Visit> visits)
        {
            this.visit_list = visits;
        }

        public override Visit this[int position] => visit_list[position];

        public override int Count => visit_list.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            Visit visit = visit_list[position];
            
            Patient patient = patient_visit.Find(i => i.IDPatient == visit.Patient);

            View view = convertView;
            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.patient_date_cell, null);
                view.Click += (sender, e) =>
                {
                    var tech_info_activity = new Intent(context, typeof(DetailsVisitActivity));
                    var b = new Bundle();
                    b.PutString("visit", JsonConvert.SerializeObject(visit_list[position]));
                    b.PutString("idtech", null);
                    tech_info_activity.PutExtras(b);
                    context.StartActivity(tech_info_activity);
                };
                view.LongClick += (sender, e) =>
                {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.SetTitle("Suppression de la visite");
                    alert.SetMessage("Voulez-vous vraiment supprimer cette visite ?");
                    alert.SetPositiveButton("Oui", async (senderAlert, args) => {
                        bool result = await MyModelAndroid.ModelInstance.DeleteVisit(visit_list[position]);

                        if (result)
                        {
                            visit_list.RemoveAt(position);
                            this.NotifyDataSetChanged();
                        }
                        else context.ShowAlert("Erreur suppresion de la visite", "Serveur inaccessible");
                    });
                    alert.SetNegativeButton("Non", (senderAlert, args) => { });
                    alert.Show();
                };
            }

            view.FindViewById<TextView>(Resource.Id.pdc_name_forname).Text = patient.Prenom+ " " + patient.Nom;
            view.FindViewById<TextView>(Resource.Id.pdc_date).Text = visit.Date;

            return view;
        }
    }
}