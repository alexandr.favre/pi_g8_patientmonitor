﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Icu.Util;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Patient_Moniteur;

namespace Patient_Moniteur_Android
{
    [Activity(Label = "DetailsVisitActivity")]
    public class DetailsVisitActivity : AppCompatActivity, DatePickerDialog.IOnDateSetListener
    {

        private Visit visit;
        private string idtech;
        private bool is_creating_new_visit;

        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.details_visit);

            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            //GET VISIT
            idtech = Intent.Extras.GetString("idtech");
            if (Intent.Extras.GetString("visit") == null)
            {
                visit = new Visit();
                is_creating_new_visit = true;
                SupportActionBar.Title = "Ajouter une visite";
            }
            else
            {
                visit = JsonConvert.DeserializeObject<Visit>(Intent.Extras.GetString("visit"));
                is_creating_new_visit = false;
                SupportActionBar.Title = "Modifier la visite";
            }

            //FILL INFO
            Spinner patient = FindViewById<Spinner>(Resource.Id.patient_spinner);
            Button confirmBtn = FindViewById<Button>(Resource.Id.conf_visit_btn);

            List<Patient> patients = await MyModelAndroid.ModelInstance.GetAllPatients();
            var patientAdapter = new ArrayAdapter<Patient>(this, Android.Resource.Layout.SimpleSpinnerItem, patients);
            patient.Adapter = patientAdapter;
            if (!is_creating_new_visit)
            {
                int selectedDoctorIndex = patients.FindIndex(i => i.IDPatient == visit.Patient);
                patient.SetSelection(selectedDoctorIndex);
            }

            //DATE PICKER
            EditText dateEditText = FindViewById<EditText>(Resource.Id.date_visit_picker);
            if (visit != null)
                dateEditText.Text = visit.Date;
            dateEditText.Click += delegate
            {
                OnClickDateEditText();
            };

            //Confirmer
            confirmBtn.Click += (sender, e) =>
            {
                visit.Patient = patients[patient.SelectedItemPosition].IDPatient.ToString();
                if (is_creating_new_visit)
                {
                    visit.Technicien = idtech;
                    MyModelAndroid.ModelInstance.AddVisit(visit);
                }
                else
                {
                    MyModelAndroid.ModelInstance.UpdateVisit(JsonConvert.DeserializeObject<Visit>(Intent.Extras.GetString("visit")),visit);
                }
                Finish();
            };
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        private void OnClickDateEditText()
        {
            var dateTimeNow = DateTime.Now;
            DatePickerDialog datePicker;
            if (is_creating_new_visit && visit.Date==null)
            {
                datePicker = new DatePickerDialog(this, this, dateTimeNow.Year, dateTimeNow.Month, dateTimeNow.Day);
            }
            else
            {
                DateTime dt = DateTime.ParseExact(visit.Date, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                datePicker = new DatePickerDialog(this, this, dt.Year, dt.Month, dt.Day);
            }

            datePicker.Show();
        }

        public void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            DateTime dt = new DateTime(year, month+1, dayOfMonth);
            visit.Date = dt.ToString("yyyy-MM-dd");
            FindViewById<EditText>(Resource.Id.date_visit_picker).Text = visit.Date;
        }
    }
}