﻿using System.Collections.Generic;
using Patient_Moniteur;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace Patient_Moniteur_Android
{
    [Activity(Label = "Planning", Theme = "@style/AppTheme")]
    public class PlanningActivity : AppCompatActivity
    {
        private PatientVisiteAdapter pva;
 
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.planning);
        }

        protected override void OnResume()
        {
            base.OnResume();

            // Get unconfirmed visits
            List<Visit> visitsList = MyModelAndroid.ModelInstance.GetUnconfirmedVisitsOffline();
            ListView planning = FindViewById<ListView>(Resource.Id.planning_list);
            if (pva == null)
            {
                pva = new PatientVisiteAdapter(visitsList, this);
                planning.ItemClick += (sender, e) =>
                {
                    var visit_patient_activity = new Intent(this, typeof(VisitPatientActivity));
                    var b = new Bundle();
                    b.PutString("visit", JsonConvert.SerializeObject(pva[e.Position]));
                    visit_patient_activity.PutExtras(b);
                    StartActivity(visit_patient_activity);
                };
            }
            else
            {
                pva.SetVisitsList(visitsList);
                pva.NotifyDataSetChanged();
            }
           
            planning.Adapter = pva;
        }

        public override void OnBackPressed()
        {
            FinishAffinity();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            this.MenuInflater.Inflate(Resource.Menu.menu_planning, menu);

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_synchro:
                    var sync_activity = new Intent(this, typeof(SyncActivity));
                    StartActivity(sync_activity);
                    break;
                case Resource.Id.menu_logout:
                    var param_activity = new Intent(this, typeof(LoginActivity));
                    MyModelAndroid.ModelInstance.ResetAccount();
                    StartActivity(param_activity);
                    Finish();
                    break;
            }
            
            return true;
        }
    }

    class PatientVisiteAdapter : BaseAdapter<Visit>
    {

        private List<Visit> visitsList;
        private PlanningActivity context;

        public PatientVisiteAdapter(List<Visit> visits, PlanningActivity context)
        {
            this.visitsList = visits;
            this.context = context;
        }

        public void SetVisitsList(List<Visit> visits){
            this.visitsList = visits;
        }

        public override Visit this[int position] => visitsList[position];

        public override int Count => visitsList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            Visit visit = visitsList[position];
            Patient patient = MyModelAndroid.ModelInstance.GetPatientOffline(visit.Patient);

            View view = convertView;
            if (view == null)
            {
                view = context.LayoutInflater.Inflate(Resource.Layout.patient_date_cell, null);
            }

            view.FindViewById<TextView>(Resource.Id.pdc_name_forname).Text = patient.Prenom + " " + patient.Nom;
            view.FindViewById<TextView>(Resource.Id.pdc_date).Text = visit.Date;

            return view;
        }
    }
}