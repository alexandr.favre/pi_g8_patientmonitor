﻿
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using AlertDialog = Android.App.AlertDialog;

namespace Patient_Moniteur_Android
{
    [Activity(Theme = "@style/AppTheme")]
    public class SyncActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.synchronisation);

            ProgressBar sync_progress = FindViewById<ProgressBar>(Resource.Id.sync_progress);
            sync_progress.Visibility = ViewStates.Gone;

            Button sync_btn = FindViewById<Button>(Resource.Id.sync_btn);
            sync_btn.Click += async (sender, e) =>
            {
                sync_progress.Visibility = ViewStates.Visible;
                bool sync_ok = await MyModelAndroid.ModelInstance.Sync();
                sync_progress.Visibility = ViewStates.Gone;
                if (!sync_ok) ShowAlert("Erreur synchronisation", "Serveur inaccessible");
            };

            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
        }

        private void ShowAlert(string title, string message)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.SetTitle(title);
            alertDialog.SetMessage(message);
            alertDialog.SetNeutralButton("OK", delegate {
                alertDialog.Dispose();
            });
            alertDialog.Show();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    Finish();
                    return true;

                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}