using Foundation;
using Patient_Moniteur;
using System;
using System.Collections.Generic;
using UIKit;

namespace Patient_Moniteur_IOS
{
    public partial class PlanningViewCtrl : UITableViewController
    {
        private List<Visit> visitsList = new List<Visit>();

        public PlanningViewCtrl(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            logoutBtn.Clicked += (object sender, EventArgs e) =>
            {
                MyModelIOS.ModelInstance.ResetAccount();
                DismissViewController(true, null);
            };
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // Get unconfirmed visits
            visitsList = MyModelIOS.ModelInstance.GetUnconfirmedVisitsOffline();
            visitsTableView.ReloadData();
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            if (segue.Identifier == "show_visit_patient"){
                VisitPatientViewCtrl destination = segue.DestinationViewController as VisitPatientViewCtrl;
                destination.visit = visitsList[visitsTableView.IndexPathForSelectedRow.Row];
            }
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nint RowsInSection(UITableView tableView, nint section)
        {
            return visitsList.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell("patient_cell", indexPath);

            Visit visit = visitsList[indexPath.Row];
            Patient patient = MyModelIOS.ModelInstance.GetPatientOffline(visit.Patient);

            cell.TextLabel.Text = patient.Prenom + " " + patient.Nom;
            cell.DetailTextLabel.Text = visit.Date;

            return cell;
        }
    }
}