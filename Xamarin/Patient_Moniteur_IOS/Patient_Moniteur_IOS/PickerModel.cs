﻿using System;
using System.Collections.Generic;
using UIKit;

namespace Patient_Moniteur_IOS
{
    public class PickerModel<T> : UIPickerViewModel
    {
        public T SelectedItem;
        private List<T> items = new List<T>();

        public PickerModel(List<T> items)
        {
            this.items = items;
        }

        public override void Selected(UIPickerView pickerView, nint row, nint component)
        {
            if (items != null)
            {
                SelectedItem = items[(int)row];
            }
        }

        public override nint GetComponentCount(UIPickerView pickerView)
        {
            return 1;
        }

        public override nint GetRowsInComponent(UIPickerView pickerView, nint component)
        {
            return items.Count;
        }

        public override string GetTitle(UIPickerView pickerView, nint row, nint component)
        {
            if (component == 0)
                return items[(int)row].ToString();
            else
                return row.ToString();
        }
    }
}
