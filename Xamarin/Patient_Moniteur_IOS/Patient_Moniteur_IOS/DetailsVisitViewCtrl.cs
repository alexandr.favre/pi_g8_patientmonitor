using System;
using System.Collections.Generic;
using Foundation;
using Patient_Moniteur;
using UIKit;

namespace Patient_Moniteur_IOS
{
    public partial class DetailsVisitViewCtrl : UIViewController
    {
        public Visit visit;
        public string technicianId;

        public DetailsVisitViewCtrl(IntPtr handle) : base(handle)
        {
        }

        public async override void ViewDidLoad()
        {
            base.ViewDidLoad();

            List<Patient> patients = await MyModelIOS.ModelInstance.GetAllPatients();
            var patientPickerModel = new PickerModel<Patient>(patients);
            patientPicker.Model = patientPickerModel;

            if (visit == null) Title = "Ajouter une visite";
            else
            {
                Title = "Modifier la visite";

                int selectedPatientIndex = patients.FindIndex(i => i.IDPatient == visit.Patient);
                patientPicker.Select(selectedPatientIndex, 0, true);

                DateTime datetime = DateTime.ParseExact(visit.Date, "yyyy-MM-dd", null);
                datePicker.Date = DateTimeToNSDate(datetime);
            }

            confirmBtn.Clicked += async (object sender, EventArgs e) =>
            {
                DateTime datetime = NSDateToDateTime(datePicker.Date);
                string date = datetime.ToString("yyyy-MM-dd");
                string idPatient = patients[(int)patientPicker.SelectedRowInComponent(0)].IDPatient;

                if (visit == null)
                {
                    bool result = await MyModelIOS.ModelInstance.AddVisit(new Visit(technicianId, idPatient, date, false));
                }
                else
                {
                    bool result = await MyModelIOS.ModelInstance.UpdateVisit(visit, new Visit(visit.Technicien, idPatient, date, visit.Confirme));
                }
                NavigationController.PopViewController(true);
            };
        }

        public static NSDate DateTimeToNSDate(DateTime date)
        {
            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));
            return NSDate.FromTimeIntervalSinceReferenceDate((date - reference).TotalSeconds);
        }

        public static DateTime NSDateToDateTime(NSDate date)
        {
            DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));
            return reference.AddSeconds(date.SecondsSinceReferenceDate);
        }
    }
}