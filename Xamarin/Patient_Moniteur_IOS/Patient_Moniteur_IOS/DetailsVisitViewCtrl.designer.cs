// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Patient_Moniteur_IOS
{
    [Register ("DetailsVisitViewCtrl")]
    partial class DetailsVisitViewCtrl
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem confirmBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIDatePicker datePicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView patientPicker { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (confirmBtn != null) {
                confirmBtn.Dispose ();
                confirmBtn = null;
            }

            if (datePicker != null) {
                datePicker.Dispose ();
                datePicker = null;
            }

            if (patientPicker != null) {
                patientPicker.Dispose ();
                patientPicker = null;
            }
        }
    }
}