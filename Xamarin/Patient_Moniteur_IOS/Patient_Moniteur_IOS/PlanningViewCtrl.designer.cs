// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Patient_Moniteur_IOS
{
    [Register ("PlanningViewCtrl")]
    partial class PlanningViewCtrl
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem logoutBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem syncBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView visitsTableView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (logoutBtn != null) {
                logoutBtn.Dispose ();
                logoutBtn = null;
            }

            if (syncBtn != null) {
                syncBtn.Dispose ();
                syncBtn = null;
            }

            if (visitsTableView != null) {
                visitsTableView.Dispose ();
                visitsTableView = null;
            }
        }
    }
}