using Foundation;
using System;
using UIKit;

namespace Patient_Moniteur_IOS
{
    public partial class SettingsViewCtrl : UIViewController
    {
        public SettingsViewCtrl(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            //récupération de l'adresse IP sauvegardé
            string savedServerIP = MyModelIOS.ModelInstance.GetServerIP();

            //affichage de l'adresse IP sauvegardé si elle existe
            if (savedServerIP != null) serverIP.Text = savedServerIP;

            // ajout d'un event listener sur le boutton "Confirmer"
            confirmBtn.Clicked += (object sender, EventArgs e) =>    
            {
                MyModelIOS.ModelInstance.SaveServerIP(serverIP.Text);// Sauvegarde de l'IP
                DismissViewController(true, null);// Fermeture de la vue
            };

            backBtn.Clicked += (object sender, EventArgs e) =>
            {
                DismissViewController(true, null);
            };
        }
    }
}