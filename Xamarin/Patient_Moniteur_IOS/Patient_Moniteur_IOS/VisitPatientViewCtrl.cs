using System;
using System.Collections.Generic;
using System.IO;
using Foundation;
using Newtonsoft.Json.Linq;
using Patient_Moniteur;
using UIKit;

namespace Patient_Moniteur_IOS
{
    public partial class VisitPatientViewCtrl : UIViewController
    {
        public Visit visit;

        private List<VisitPicture> pictures = new List<VisitPicture>();

        public VisitPatientViewCtrl(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Patient patient = MyModelIOS.ModelInstance.GetPatientOffline(visit.Patient);

            var addressJson = JObject.Parse(patient.Adresse);
            lastName.Text = patient.Nom;
            firstName.Text = patient.Prenom;
            dateVisit.Text = visit.Date;
            address.Text = addressJson["adresse"].ToString();
            city.Text = addressJson["ville"].ToString();
            phone.Text = patient.NumTel;

            List<Doctor> doctors = MyModelIOS.ModelInstance.GetAllDoctorsOffline();
            var doctorPickerModel = new PickerModel<Doctor>(doctors);
            doctorPicker.Model = doctorPickerModel;
            int selectedDoctorIndex = doctors.FindIndex(i => i.IDMedecin == patient.Medecin);
            doctorPicker.Select(selectedDoctorIndex, 0, true);

            List<Device> devices = MyModelIOS.ModelInstance.GetAllDevicesOffline();
            var devicePickerModel = new PickerModel<Device>(devices);
            devicePicker.Model = devicePickerModel;
            int selectedDeviceIndex = devices.FindIndex(i => i.NumAppareil == patient.Appareil);
            devicePicker.Select(selectedDeviceIndex, 0, true);

            Title = patient.Prenom + " " + patient.Nom;

            addImageBtn.TouchUpInside += async (object sender, EventArgs e) =>
            {
                PicturePicker picturePicker = new PicturePicker(this);
                Stream picture = await picturePicker.GetImageAsync();
                pictures.Add(new VisitPicture(visit.Patient, visit.Date, StreamExtensions.ToByteArray(picture)));
                if (pictures.Count > 0) selectedImagesLbl.Text = "[" + pictures.Count + " image(s) sélectionnée(s)]";
            };

            confirmBtn.TouchUpInside += (object sender, EventArgs e) =>
            {
                Patient updatedPatient = new Patient(
                    patient.IDPatient,
                    doctors[(int)doctorPicker.SelectedRowInComponent(0)].IDMedecin,
                    devices[(int)devicePicker.SelectedRowInComponent(0)].NumAppareil,
                    lastName.Text,
                    firstName.Text,
                    "{ 'adresse': '" + address.Text + "', 'ville': '" + city.Text + "' }",
                    phone.Text,
                    patient.Timestamp);

                if (patient.Nom != updatedPatient.Nom ||
                    patient.Prenom != updatedPatient.Prenom ||
                    patient.Medecin != updatedPatient.Medecin ||
                    patient.Appareil != updatedPatient.Appareil ||
                    patient.Adresse != updatedPatient.Adresse ||
                    patient.NumTel != updatedPatient.NumTel)
                {
                    MyModelIOS.ModelInstance.UpdatePatientOffline(updatedPatient);
                }
                MyModelIOS.ModelInstance.UpdateVisitOffline(new Visit(visit.Id, visit.Technicien, visit.Patient, visit.Date, true, visit.Timestamp));
                if (pictures.Count > 0) MyModelIOS.ModelInstance.AddVisitPicturesOffline(pictures);
                NavigationController.PopViewController(true);
            };
        }
    }
}