﻿using System;
using Patient_Moniteur;
using System.IO;

namespace Patient_Moniteur_IOS
{
    class MyModelIOS : MyModel
    {
        private static MyModelIOS _ModelInstance;

        private static string Connection
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), SQLiteHelper.DATABASENAME);
            }
        }

        public static MyModelIOS ModelInstance
        {
            get
            {
                if (_ModelInstance == null)
                {
                    _ModelInstance = new MyModelIOS();
                }
                return _ModelInstance;
            }
        }

        private MyModelIOS() : base(Connection)
        {

        }
    }
}