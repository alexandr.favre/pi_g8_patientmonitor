using Foundation;
using Patient_Moniteur;
using System;
using System.Collections.Generic;
using UIKit;

namespace Patient_Moniteur_IOS
{
    public partial class TechniciansViewCtrl : UITableViewController
    {
        private List<Technician> techniciansList = new List<Technician>();

        public TechniciansViewCtrl(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            logoutBtn.Clicked += (object sender, EventArgs e) =>
            {
                MyModelIOS.ModelInstance.ResetAccount();
                DismissViewController(true, null);
            };
        }

        public async override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // Get technicians
            techniciansList = await MyModelIOS.ModelInstance.GetAllTechnicians();

            if (techniciansList == null)
            {
                techniciansList = new List<Technician>();
                ShowAlert("Erreur récupération des techniciens", "Serveur inaccessible");
            }
            techniciansTableView.ReloadData();
        }

        private void ShowAlert(string title, string message)
        {
            var okAlertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
            PresentViewController(okAlertController, true, null);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            if (segue.Identifier == "show_technician_infos")
            {
                TechnicianInfosViewCtrl destination = segue.DestinationViewController as TechnicianInfosViewCtrl;
                destination.technician = techniciansList[techniciansTableView.IndexPathForSelectedRow.Row];
            }
        }

        public override nint NumberOfSections(UITableView tableView)
        {
            return 1;
        }

        public override nint RowsInSection(UITableView tableView, nint section)
        {
            return techniciansList.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell cell = tableView.DequeueReusableCell("technician_cell", indexPath);

            Technician technician = techniciansList[indexPath.Row];

            cell.TextLabel.Text = technician.Prenom + " " + technician.Nom;

            return cell;
        }
    }
}