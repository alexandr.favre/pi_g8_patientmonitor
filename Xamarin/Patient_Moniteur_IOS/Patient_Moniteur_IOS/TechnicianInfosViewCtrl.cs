using Foundation;
using Newtonsoft.Json.Linq;
using Patient_Moniteur;
using System;
using System.Collections.Generic;
using UIKit;

namespace Patient_Moniteur_IOS
{
    public partial class TechnicianInfosViewCtrl : UIViewController
    {
        public Technician technician;
        private VisitsTableViewSource visitsTableViewSource;

        public TechnicianInfosViewCtrl(IntPtr handle) : base(handle)
        {
        }

        public async override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var addressJson = JObject.Parse(technician.Adresse);
            lastName.Text = technician.Nom;
            firstName.Text = technician.Prenom;
            address.Text = addressJson["adresse"].ToString();
            city.Text = addressJson["ville"].ToString();
            phone.Text = technician.NumTel;

            Title = technician.Prenom + " " + technician.Nom;

            confirmBtn.TouchUpInside += async (object sender, EventArgs e) =>
            {
                Technician updatedTechnician =
                  new Technician(technician.IDTechnicien,
                                 lastName.Text,
                                 firstName.Text,
                                 "{ 'adresse': '" + address.Text + "', 'ville': '" + city.Text + "' }",
                                 phone.Text);
                bool result = await MyModelIOS.ModelInstance.UpdateTechncian(updatedTechnician);
                if (result)
                {
                    ShowAlert("Mise à jour du technicien", "Les informations du technicien ont correctement été mises à jour");
                    Title = updatedTechnician.Prenom + " " + updatedTechnician.Nom;
                }
                else ShowAlert("Erreur mise à jour du technicien", "Serveur inaccessible");
            };
        }

        public async override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            // Get technician's planning
            string planningJson = await MyModelIOS.ModelInstance.GetPlanningOfTechnician(technician.IDTechnicien);

            if (planningJson == null) ShowAlert("Erreur récupération du planning", "Serveur inaccessible");
            else
            {
                // Convert json to lists
                var root = JObject.Parse(planningJson);
                List<Visit> visits = root["visiter"].ToObject<List<Visit>>();
                List<Patient> patients = root["patient"].ToObject<List<Patient>>();

                visitsTableViewSource = new VisitsTableViewSource(this, visits, patients);
                visitsTableView.Source = visitsTableViewSource;
                visitsTableView.ReloadData();
            }
        }

        public void ShowAlert(string title, string message)
        {
            var okAlertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
            PresentViewController(okAlertController, true, null);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            if (segue.Identifier == "show_visit_update")
            {
                DetailsVisitViewCtrl destination = segue.DestinationViewController as DetailsVisitViewCtrl;
                destination.visit = visitsTableViewSource.visitsList[visitsTableView.IndexPathForSelectedRow.Row];
            } else if (segue.Identifier == "show_visit_add")
            {
                DetailsVisitViewCtrl destination = segue.DestinationViewController as DetailsVisitViewCtrl;
                destination.technicianId = technician.IDTechnicien;
            }
        }

        public class VisitsTableViewSource : UITableViewSource
        {
            private TechnicianInfosViewCtrl viewCtrl;
            public List<Visit> visitsList;
            public List<Patient> patientsList;

            public VisitsTableViewSource(TechnicianInfosViewCtrl viewCtrl, List<Visit> visitsList, List<Patient> patientsList)
            {
                this.viewCtrl = viewCtrl;
                this.visitsList = visitsList;
                this.patientsList = patientsList;
            }

            public override nint RowsInSection(UITableView tableView, nint section)
            {
                return visitsList.Count;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                UITableViewCell cell = tableView.DequeueReusableCell("visit_cell", indexPath);

                Visit visit = visitsList[indexPath.Row];
                Patient patient = patientsList.Find(i => i.IDPatient == visit.Patient);

                cell.TextLabel.Text = patient.Prenom + " " + patient.Nom;
                cell.DetailTextLabel.Text = visit.Date;

                return cell;
            }

            public async override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, Foundation.NSIndexPath indexPath)
            {
                switch (editingStyle)
                {
                    case UITableViewCellEditingStyle.Delete:
                        bool result = await MyModelIOS.ModelInstance.DeleteVisit(visitsList[indexPath.Row]);
                        if (result)
                        {
                            visitsList.RemoveAt(indexPath.Row);
                            tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
                        }
                        else viewCtrl.ShowAlert("Erreur suppresion de la visite", "Serveur inaccessible");
                        break;
                }
            }

            public override string TitleForDeleteConfirmation(UITableView tableView, NSIndexPath indexPath)
            {
                return "Supprimer";
            }
        }
    }
}