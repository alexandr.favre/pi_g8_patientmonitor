using System;
using UIKit;
using Patient_Moniteur;
using System.Threading;

namespace Patient_Moniteur_IOS
{
    public partial class LoginViewCtrl : UIViewController
    {
        public LoginViewCtrl(IntPtr handle) : base(handle)
        {
        }

        // Docker : localhost | Toolbox : 192.168.99.100

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            string savedEmail = MyModelIOS.ModelInstance.GetEmailValue();
            if (savedEmail != null) emailTxt.Text = savedEmail;

            loginBtn.TouchUpInside += async (object sender, EventArgs e) =>
            {
                emailTxt.Enabled = false;
                passwordTxt.Enabled = false;
                string identifierValue = await MyModelIOS.ModelInstance.Login(emailTxt.Text, passwordTxt.Text);
                emailTxt.Enabled = true;
                passwordTxt.Enabled = true;
                LoginRedirection(identifierValue);
                if (identifierValue == "0") ShowAlert("Erreur login", "Identifiants incorrects");
                else if (identifierValue == null) ShowAlert("Erreur login", "Serveur inaccessible");
            };
        }

        private void ShowAlert(string title, string message)
        {
            var okAlertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
            PresentViewController(okAlertController, true, null);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            LoginRedirection(MyModelIOS.ModelInstance.GetIdentifierValue());
        }

        private async void LoginRedirection(string identifierValue)
        {
            if (identifierValue == "1") PerformSegue("technicianShow", this);
            else if (identifierValue == "2")
            {
                if (await MyModelIOS.ModelInstance.IsLogged()) PerformSegue("respTechShow", this);
            }
        }
    }
}