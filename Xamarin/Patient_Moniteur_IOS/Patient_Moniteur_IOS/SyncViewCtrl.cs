using System;
using UIKit;

namespace Patient_Moniteur_IOS
{
    public partial class SyncViewCtrl : UIViewController
    {
        public SyncViewCtrl(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            progress_sync_top_constraint.Constant = 0;
            sync_progress.Hidden = true;

            sync_btn.TouchUpInside += async (object sender, EventArgs e) =>
            {
                progress_sync_top_constraint.Constant = 50;
                sync_progress.Hidden = false;
                sync_progress.StartAnimating();
                bool sync_ok = await MyModelIOS.ModelInstance.Sync();
                progress_sync_top_constraint.Constant = 0;
                sync_progress.Hidden = true;
                sync_progress.StopAnimating();
                if (!sync_ok) ShowAlert("Erreur synchronisation", "Serveur inaccessible");
            };
        }

        private void ShowAlert(string title, string message)
        {
            var okAlertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));
            PresentViewController(okAlertController, true, null);
        }
    }
}