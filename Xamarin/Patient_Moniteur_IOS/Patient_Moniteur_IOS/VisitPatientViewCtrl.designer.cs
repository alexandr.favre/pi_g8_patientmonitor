// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Patient_Moniteur_IOS
{
    [Register ("VisitPatientViewCtrl")]
    partial class VisitPatientViewCtrl
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton addImageBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField address { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField city { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton confirmBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField dateVisit { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView devicePicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIPickerView doctorPicker { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField firstName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField lastName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField phone { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel selectedImagesLbl { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (addImageBtn != null) {
                addImageBtn.Dispose ();
                addImageBtn = null;
            }

            if (address != null) {
                address.Dispose ();
                address = null;
            }

            if (city != null) {
                city.Dispose ();
                city = null;
            }

            if (confirmBtn != null) {
                confirmBtn.Dispose ();
                confirmBtn = null;
            }

            if (dateVisit != null) {
                dateVisit.Dispose ();
                dateVisit = null;
            }

            if (devicePicker != null) {
                devicePicker.Dispose ();
                devicePicker = null;
            }

            if (doctorPicker != null) {
                doctorPicker.Dispose ();
                doctorPicker = null;
            }

            if (firstName != null) {
                firstName.Dispose ();
                firstName = null;
            }

            if (lastName != null) {
                lastName.Dispose ();
                lastName = null;
            }

            if (phone != null) {
                phone.Dispose ();
                phone = null;
            }

            if (selectedImagesLbl != null) {
                selectedImagesLbl.Dispose ();
                selectedImagesLbl = null;
            }
        }
    }
}