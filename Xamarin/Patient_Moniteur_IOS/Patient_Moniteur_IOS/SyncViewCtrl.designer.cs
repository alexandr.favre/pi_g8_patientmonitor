// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Patient_Moniteur_IOS
{
    [Register ("SyncViewCtrl")]
    partial class SyncViewCtrl
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.NSLayoutConstraint progress_sync_top_constraint { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton sync_btn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIActivityIndicatorView sync_progress { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (progress_sync_top_constraint != null) {
                progress_sync_top_constraint.Dispose ();
                progress_sync_top_constraint = null;
            }

            if (sync_btn != null) {
                sync_btn.Dispose ();
                sync_btn = null;
            }

            if (sync_progress != null) {
                sync_progress.Dispose ();
                sync_progress = null;
            }
        }
    }
}