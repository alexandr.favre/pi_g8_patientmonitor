// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Patient_Moniteur_IOS
{
    [Register ("LoginViewCtrl")]
    partial class LoginViewCtrl
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField emailTxt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton loginBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField passwordTxt { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (emailTxt != null) {
                emailTxt.Dispose ();
                emailTxt = null;
            }

            if (loginBtn != null) {
                loginBtn.Dispose ();
                loginBtn = null;
            }

            if (passwordTxt != null) {
                passwordTxt.Dispose ();
                passwordTxt = null;
            }
        }
    }
}