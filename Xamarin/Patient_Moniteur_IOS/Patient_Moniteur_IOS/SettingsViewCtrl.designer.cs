// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Patient_Moniteur_IOS
{
    [Register ("SettingsViewCtrl")]
    partial class SettingsViewCtrl
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem backBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem confirmBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField serverIP { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (backBtn != null) {
                backBtn.Dispose ();
                backBtn = null;
            }

            if (confirmBtn != null) {
                confirmBtn.Dispose ();
                confirmBtn = null;
            }

            if (serverIP != null) {
                serverIP.Dispose ();
                serverIP = null;
            }
        }
    }
}