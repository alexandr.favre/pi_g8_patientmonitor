﻿using SQLite;

namespace Patient_Moniteur
{
    [Table("Medecin")]
    public class Doctor
    {
        [PrimaryKey]
        public string IDMedecin { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }
        public string NumTel { get; set; }

        public Doctor() { }

        public Doctor(string iDMedecin, string nom, string prenom, string adresse, string numTel)
        {
            IDMedecin = iDMedecin;
            Nom = nom;
            Prenom = prenom;
            Adresse = adresse;
            NumTel = numTel;
        }

        public override string ToString()
        {
            return Prenom + " " + Nom;
        }
    }
}
