﻿using System;
using SQLite;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Patient_Moniteur
{
    public class SQLiteHelper
    {
        public const string DATABASENAME = "PatientMoniteur.db";

        private SQLiteConnection connection;

        public SQLiteHelper(string path)
        {
            connection = new SQLiteConnection(path);

            // Create tables if not exist
            connection.CreateTable<Doctor>();
            connection.CreateTable<Visit>();
            connection.CreateTable<Device>();
            connection.CreateTable<Patient>();
            connection.CreateTable<VisitPicture>();
            connection.CreateTable<Operation>();
        }

        public void FlushTable(string table)
        {
            try
            {
                connection.Execute("delete from " + table);
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public bool ReplaceDatabase(string data)
        {

            // Flush the database
            FlushTable("Medecin");
            FlushTable("Patient");
            FlushTable("Visiter");
            FlushTable("Appareil");
            FlushTable("PhotoVisiter");
            FlushTable("Operation");

            var root = JObject.Parse(data);

            // Convert json to lists
            List<Device> devices = root["appareil"].ToObject<List<Device>>();
            List<Doctor> doctors = root["medecin"].ToObject<List<Doctor>>();
            List<Patient> patients = root["patient"].ToObject<List<Patient>>();
            List<Visit> visits = root["visiter"].ToObject<List<Visit>>();

            // Insert data
            try
            {
                foreach (Device device in devices) connection.Insert(device);
                foreach (Doctor doctor in doctors) connection.Insert(doctor);
                foreach (Patient patient in patients) connection.Insert(patient);
                foreach (Visit visit in visits) connection.Insert(visit);
                return true;
            }
            catch (SQLiteException e)
            {
                Console.WriteLine(e.ToString());
                return false;
            }
        }

        public List<Doctor> GetAllDoctors()
        {
            return connection.Table<Doctor>().ToList();
        }

        public List<Device> GetAllDevices()
        {
            return connection.Table<Device>().ToList();
        }

        public List<Visit> GetUnconfirmedVisits()
        {
            return connection.Table<Visit>().Where(x => x.Confirme == false).ToList();
        }

        public Doctor GetDoctor(string id)
        {
            return connection.Table<Doctor>().Where(x => x.IDMedecin == id).FirstOrDefault();
        }

        public Device GetDevice(string id)
        {
            return connection.Table<Device>().Where(x => x.NumAppareil == id).FirstOrDefault();
        }

        public Patient GetPatient(string id)
        {
            return connection.Table<Patient>().Where(x => x.IDPatient == id).FirstOrDefault();
        }

        public void UpdatePatient(Patient patient)
        {
            connection.Update(patient);
        }

        public void UpdateVisit(Visit visit)
        {
            connection.Update(visit);
        }

        public void AddVisitPictures(List<VisitPicture> pictures)
        {
            foreach (VisitPicture picture in pictures)
            {
                connection.Insert(picture);
            }
        }

        public List<VisitPicture> GetAllPictures()
        {
            return connection.Table<VisitPicture>().ToList();
        }

        public void SaveOperation<T>(T obj)
        {
            if (obj.GetType() == typeof(Patient))
            {
                Patient patient = obj as Patient;
                connection.Insert(patient.ToOperation());
            }
            else if (obj.GetType() == typeof(Visit))
            {
                Visit visit = obj as Visit;
                connection.Insert(visit.ToOperation());
            }
        }

        public List<Operation> GetOperations()
        {
            return connection.Table<Operation>().ToList();
        }
    }
}
