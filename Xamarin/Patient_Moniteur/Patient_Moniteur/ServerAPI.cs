﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Patient_Moniteur
{
    public class ServerAPI
    {
        public static string SERVER_IP;

        public ServerAPI(string serverIP)
        {
            SERVER_IP = serverIP;
        }

        public async Task<JObject> Login(string email, string password)
        {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            JObject result = null;

            var provider = MD5.Create();
            string salt = "p@t!3nt";
            byte[] bytes = provider.ComputeHash(Encoding.ASCII.GetBytes(salt + password));
            string passwordHashed = BitConverter.ToString(bytes).Replace("-", "").ToLower();

            var content = new StringContent(
                JsonConvert.SerializeObject(new { email = email, password = passwordHashed }),
                Encoding.UTF8, "application/json");

            try
            {
                response = await client.PostAsync("http://" + SERVER_IP + ":8080/login", content);

                if (response.IsSuccessStatusCode)
                {
                    var json = response.Content.ReadAsStringAsync().Result;

                    result = JObject.Parse(json);
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return result;
        }

        public async Task<bool> IsLogged(string token)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            bool result = false;

            try
            {
                response = await client.GetAsync("http://" + SERVER_IP + ":8080/is_logged");

                if (response.IsSuccessStatusCode)
                {
                    Boolean.TryParse(response.Content.ReadAsStringAsync().Result, out result);
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return result;
        }

        public async Task<string> Sync(string token, string email, List<Operation> operations)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            string result = null;

            var content = new StringContent(
                JsonConvert.SerializeObject(new { email = email, operations = operations }),
                Encoding.UTF8, "application/json");

            try
            {
                response = await client.PostAsync("http://" + SERVER_IP + ":8080/sync", content);

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return result;
        }

        public async Task<List<Technician>> GetAllTechnicians(string token)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            List<Technician> result = null;

            try
            {
                response = await client.GetAsync("http://" + SERVER_IP + ":8080/technicians");

                if (response.IsSuccessStatusCode)
                {
                    result = JsonConvert.DeserializeObject<List<Technician>>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return result;
        }

        public async Task<string> GetPlanningOfTechnician(string token, string id)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            string result = null;

            var content = new StringContent(
                JsonConvert.SerializeObject(new { idTechnicien = id }),
                Encoding.UTF8, "application/json");

            try
            {
                response = await client.PostAsync("http://" + SERVER_IP + ":8080/planning", content);

                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return result;
        }

        public async Task<bool> UpdateTechnician(string token, Technician technician)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            bool success = false;

            var content = new StringContent(JsonConvert.SerializeObject(technician), Encoding.UTF8, "application/json");

            try
            {
                response = await client.PostAsync("http://" + SERVER_IP + ":8080/update_technician", content);

                if (response.IsSuccessStatusCode)
                {
                    response.Content.ReadAsStringAsync();
                    success = true;
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return success;
        }

        public async Task<bool> DeleteVisit(string token, Visit visit)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            bool success = false;

            var content = new StringContent(JsonConvert.SerializeObject(visit), Encoding.UTF8, "application/json");

            try
            {
                response = await client.PostAsync("http://" + SERVER_IP + ":8080/delete_visit", content);

                if (response.IsSuccessStatusCode)
                {
                    response.Content.ReadAsStringAsync();
                    success = true;
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return success;
        }

        public async Task<List<Patient>> GetAllPatients(string token)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            List<Patient> result = null;

            try
            {
                response = await client.GetAsync("http://" + SERVER_IP + ":8080/patients");

                if (response.IsSuccessStatusCode)
                {
                    result = JsonConvert.DeserializeObject<List<Patient>>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return result;
        }

        public async Task<bool> UpdateVisit(string token, Visit oldVisit, Visit newVisit)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            bool success = false;

            var content = new StringContent(
                JsonConvert.SerializeObject(new { oldVisit = oldVisit, newVisit = newVisit }),
                Encoding.UTF8,
                "application/json"
            );

            try
            {
                response = await client.PostAsync("http://" + SERVER_IP + ":8080/update_visit", content);

                if (response.IsSuccessStatusCode)
                {
                    response.Content.ReadAsStringAsync();
                    success = true;
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return success;
        }

        public async Task<bool> AddVisit(string token, Visit visit)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            bool success = false;

            var content = new StringContent(JsonConvert.SerializeObject(visit), Encoding.UTF8, "application/json");

            try
            {
                response = await client.PostAsync("http://" + SERVER_IP + ":8080/add_visit", content);

                if (response.IsSuccessStatusCode)
                {
                    response.Content.ReadAsStringAsync();
                    success = true;
                }
            }
            catch (Exception e)
            {
                // Ne rien faire
            }

            return success;
        }

        public async Task<bool> UploadPictures(string token, List<VisitPicture> pictures)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            client.Timeout = TimeSpan.FromSeconds(5);
            HttpResponseMessage response;
            bool success = false;

            foreach (VisitPicture visitPicture in pictures)
            {
                var content = new MultipartFormDataContent();
                ByteArrayContent bytesArray = new ByteArrayContent(visitPicture.Picture);
                content.Add(new StringContent(visitPicture.Patient), "Patient");
                content.Add(new StringContent(visitPicture.Date), "Date");
                content.Add(bytesArray, "file", "picture.jpg");

                try
                {
                    response = await client.PostAsync("http://" + SERVER_IP + ":8080/upload_image", content);

                    if (response.IsSuccessStatusCode)
                    {
                        response.Content.ReadAsStringAsync();
                        success = true;
                    }
                }
                catch (Exception e)
                {
                    // Ne rien faire
                }
            }

            return success;
        }
    }
}
