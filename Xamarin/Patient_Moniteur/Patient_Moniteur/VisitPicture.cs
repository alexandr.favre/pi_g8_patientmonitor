﻿using System;
using System.IO;
using SQLite;

namespace Patient_Moniteur
{
    [Table("PhotoVisiter")]
    public class VisitPicture
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Patient { get; set; }
        public string Date { get; set; }
        public byte[] Picture { get; set; }

        public VisitPicture(){}

        public VisitPicture(string patient, string date, byte[] picture)
        {
            Patient = patient;
            Date = date;
            Picture = picture;
        }
    }
}
