﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Patient_Moniteur
{
    public class Properties
    {
        private const string EMAIL_VALUE_NAME = "email";
        private const string TOKEN_VALUE_NAME = "token";
        private const string SERVER_IP_VALUE_NAME = "serverIP";
        private const string IDENTIFIER_VALUE_NAME = "identifierValue";

        private ISettings AppSettings => CrossSettings.Current;

        public void SaveServerIP(string serverIP)
        {
            AppSettings.AddOrUpdateValue(SERVER_IP_VALUE_NAME, serverIP);
            ServerAPI.SERVER_IP = serverIP;
        }

        public string GetServerIP()
        {
            return AppSettings.GetValueOrDefault(SERVER_IP_VALUE_NAME, "localhost");
        }

        public void SaveAccount(string email, string token, string identifierValue)
        {
            AppSettings.AddOrUpdateValue(EMAIL_VALUE_NAME, email);
            AppSettings.AddOrUpdateValue(TOKEN_VALUE_NAME, token);
            AppSettings.AddOrUpdateValue(IDENTIFIER_VALUE_NAME, identifierValue);
        }

        public string GetIdentifierValue()
        {
            return AppSettings.GetValueOrDefault(IDENTIFIER_VALUE_NAME, "0");
        }

        public string GetEmailValue()
        {
            return AppSettings.GetValueOrDefault(EMAIL_VALUE_NAME, null);
        }

        public string GetTokenValue()
        {
            return AppSettings.GetValueOrDefault(TOKEN_VALUE_NAME, null);
        }

        public void ResetAccount()
        {
            AppSettings.AddOrUpdateValue(IDENTIFIER_VALUE_NAME, "0");
            AppSettings.AddOrUpdateValue(TOKEN_VALUE_NAME, null);
        }
    }
}
