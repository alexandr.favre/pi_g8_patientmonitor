﻿using SQLite;

namespace Patient_Moniteur
{
    [Table("Operation")]
    public class Operation
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string P1 { get; set; }
        public string P2 { get; set; }
        public string Table { get; set; }
        public int Timestamp { get; set; }
        public string Request { get; set; }

        public Operation() { }

        public Operation(string p1, string p2, string table, int timestamp, string request)
        {
            P1 = p1;
            P2 = p2;
            Table = table;
            Timestamp = timestamp;
            Request = request;
        }
    }
}
