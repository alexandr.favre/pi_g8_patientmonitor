﻿using SQLite;

namespace Patient_Moniteur
{
    public class Technician
    {
        public string IDTechnicien { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }
        public string NumTel { get; set; }

        public Technician() { }

        public Technician(string iDTechnicien, string nom, string prenom, string adresse, string numTel)
        {
            IDTechnicien = iDTechnicien;
            Nom = nom;
            Prenom = prenom;
            Adresse = adresse;
            NumTel = numTel;
        }

        public override string ToString()
        {
            return Prenom + " " + Nom;
        }
    }
}
