﻿using SQLite;

namespace Patient_Moniteur
{
    /// <summary>
    /// The <c>Device</c> class.
    /// Defines the business object devices installed at patient home.
    /// </summary>
    [Table("Appareil")]
    public class Device
    {
        /// <summary>
        /// ID that uniquely identifies this <see cref="Device"/> instance.
        /// </summary>
        [PrimaryKey]
        public string NumAppareil { get; set; }
        /// <summary>
        /// Type of this <see cref="Device"/> instance.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Device() { }

        /// <summary>
        /// Creates a new instance of the <see cref="Device"/> class.
        /// </summary>
        /// <param name="numAppareil">Identifiant de l'appareil.</param>
        /// <param name="type">Type de l'appareil.</param>
        public Device(string numAppareil, string type)
        {
            NumAppareil = numAppareil;
            Type = type;
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return Type+" - #"+NumAppareil;
        }
    }
}
