﻿using SQLite;
using System;
using System.Text;

namespace Patient_Moniteur
{
    [Table("Patient")]
    public class Patient
    {
        [PrimaryKey]
        public string IDPatient { get; set; }
        public string Medecin { get; set; }
        public string Appareil { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Adresse { get; set; }
        public string NumTel { get; set; }
        public int Timestamp { get; set; }

        public Patient() { }

        public Patient(string iDPatient, string medecin, string appareil, string nom, string prenom, string adresse, string numTel, int timestamp)
        {
            IDPatient = iDPatient;
            Medecin = medecin;
            Appareil = appareil;
            Nom = nom;
            Prenom = prenom;
            Adresse = adresse;
            NumTel = numTel;
            Timestamp = timestamp;
        }

        public override string ToString()
        {
            return Prenom + " " + Nom;
        }

        public Operation ToOperation()
        {
            
            return new Operation(IDPatient, "0", "Patient", Timestamp, "UPDATE Patient SET Medecin='" + Medecin + 
                                 "', Appareil='" + Appareil + "', Nom='" + Nom + "', Prenom='" + Prenom + 
                                 "', Adresse='" + GetEscapedString(Adresse) + "', NumTel='" + NumTel + 
                                 "' WHERE IDPatient='" + IDPatient + "'");
        }

        private string GetEscapedString(string s)
        {
            char[] chars = s.ToCharArray();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < chars.Length; i++)
            {
                switch (chars[i])
                {
                    case '\'':
                        sb.Append("\\'"); break;
                    default:
                        sb.Append(chars[i]); break;
                }
            }
            return sb.ToString();
        }
    }
}
