﻿using SQLite;

namespace Patient_Moniteur
{
    [Table("Visiter")]
    public class Visit
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Technicien { get; set; }
        public string Patient { get; set; }
        public string Date { get; set; }
        public bool Confirme { get; set; }
        public int Timestamp { get; set; }

        public Visit() { }

        public Visit(string technicien, string patient, string date, bool confirme)
        {
            Technicien = technicien;
            Patient = patient;
            Date = date;
            Confirme = confirme;
            Timestamp = 0;
        }

        public Visit(int id, string technicien, string patient, string date, bool confirme, int timestamp)
        {
            Id = id;
            Technicien = technicien;
            Patient = patient;
            Date = date;
            Confirme = confirme;
            Timestamp = timestamp;
        }

        public Operation ToOperation()
        {
            return new Operation(Patient, Date, "Visiter", Timestamp,
                                 "UPDATE Visiter SET Confirme='" + (Confirme ? "1":"0") +
                                 "' WHERE Patient='" + Patient + "' AND Date='" + Date + "'");
        }
    }
}
