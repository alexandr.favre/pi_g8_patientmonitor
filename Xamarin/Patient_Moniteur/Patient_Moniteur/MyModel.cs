﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Patient_Moniteur
{
    public class MyModel
    {
        private ServerAPI serverAPI;
        private SQLiteHelper database;
        private Properties properties;

        protected MyModel(string path)
        {
            database = new SQLiteHelper(path);
            properties = new Properties();
            serverAPI = new ServerAPI(properties.GetServerIP());
        }


        // ----- ServerAPI -----

        public async Task<string> Login(string email, string password)
        {
            JObject result = await serverAPI.Login(email, password);
            if (result == null) return null;
            string token = result["token"].ToString();
            string identifierValue = result["identifierValue"].ToString();

            if (token != null && (identifierValue == "1" || identifierValue == "2"))
            {
                properties.SaveAccount(email, token, identifierValue);
            }

            return identifierValue;
        }

        public async Task<bool> IsLogged()
        {
            return await serverAPI.IsLogged(properties.GetTokenValue());
        }

        public async Task<bool> Sync()
        {
            bool uploadIsOk = await serverAPI.UploadPictures(properties.GetTokenValue(), database.GetAllPictures());
            string data = await serverAPI.Sync(properties.GetTokenValue(), properties.GetEmailValue(), database.GetOperations());
            if (data != null) database.ReplaceDatabase(data);
            return (data != null);
        }

        public async Task<List<Technician>> GetAllTechnicians()
        {
            return await serverAPI.GetAllTechnicians(properties.GetTokenValue());
        }

        public async Task<string> GetPlanningOfTechnician(string id)
        {
            return await serverAPI.GetPlanningOfTechnician(properties.GetTokenValue(), id);
        }

        public async Task<bool> UpdateTechncian(Technician technician)
        {
            return await serverAPI.UpdateTechnician(properties.GetTokenValue(), technician);
        }

        public async Task<bool> DeleteVisit(Visit visit)
        {
            return await serverAPI.DeleteVisit(properties.GetTokenValue(), visit);
        }

        public async Task<List<Patient>> GetAllPatients()
        {
            return await serverAPI.GetAllPatients(properties.GetTokenValue());
        }

        public async Task<bool> AddVisit(Visit visit)
        {
            return await serverAPI.AddVisit(properties.GetTokenValue(), visit);
        }

        public async Task<bool> UpdateVisit(Visit oldVisit, Visit newVisit)
        {
            return await serverAPI.UpdateVisit(properties.GetTokenValue(), oldVisit, newVisit);
        }


        // ----- SQLiteHelper

        public List<Visit> GetUnconfirmedVisitsOffline()
        {
            return database.GetUnconfirmedVisits();
        }

        public List<Doctor> GetAllDoctorsOffline()
        {
            return database.GetAllDoctors();
        }

        public List<Device> GetAllDevicesOffline()
        {
            return database.GetAllDevices();
        }

        public Patient GetPatientOffline(string id)
        {
            return database.GetPatient(id);
        }

        public void UpdatePatientOffline(Patient patient)
        {
            database.SaveOperation(patient);
            database.UpdatePatient(patient);
        }

        public void UpdateVisitOffline(Visit visit)
        {
            database.SaveOperation(visit);
            database.UpdateVisit(visit);
        }

        public void AddVisitPicturesOffline(List<VisitPicture> pictures)
        {
            database.AddVisitPictures(pictures);
        }


        // ----- Properties -----

        public void SaveServerIP(string serverIP)
        {
            properties.SaveServerIP(serverIP);
        }

        public string GetServerIP()
        {
            return properties.GetServerIP();
        }

        public string GetIdentifierValue()
        {
            return properties.GetIdentifierValue();
        }

        public string GetEmailValue()
        {
            return properties.GetEmailValue();
        }

        public void ResetAccount()
        {
            properties.ResetAccount();
        }
    }
}
