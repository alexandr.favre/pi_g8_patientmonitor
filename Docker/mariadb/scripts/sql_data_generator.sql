USE "mydb"

INSERT INTO Medecin (	IDMedecin	,	Nom	,	Prenom	,	Adresse	,	NumTel	) 	VALUES(	1	,"Doe","John","{ 'adresse': 'Avenue des Binchs 14', 'ville': 'Matran' }","0786659898");
INSERT INTO Medecin (	IDMedecin	,	Nom	,	Prenom	,	Adresse	,	NumTel	) 	VALUES(	2	,"Bernasconi","Maria","{ 'adresse': 'Avenue des Binchs 17', 'ville': 'Matran' }","0792314567");
INSERT INTO Medecin (	IDMedecin	,	Nom	,	Prenom	,	Adresse	,	NumTel	) 	VALUES(	3	,"Allen","Barry","{ 'adresse': 'Avenue des Binchs 19', 'ville': 'Matran' }","0786884455");

INSERT INTO Appareil (	NumAppareil	,	Type	) 		VALUES(	1	,"S8");
INSERT INTO Appareil (	NumAppareil	,	Type	) 		VALUES(	2	,"S9");
INSERT INTO Appareil (	NumAppareil	,	Type	) 		VALUES(	3	,"S10");

INSERT INTO Patient (	IDPatient	,	Medecin	,	Appareil	,	Nom	,	Prenom	,	Adresse	,	NumTel	) 	VALUES(	1	,	1	,	3	,"Favre","Alexandre","{ 'adresse': 'Chemin Pauvre Jacques 18', 'ville': 'Bulle' }","0799035708");
INSERT INTO Patient (	IDPatient	,	Medecin	,	Appareil	,	Nom	,	Prenom	,	Adresse	,	NumTel	) 	VALUES(	2	,	2	,	2	,"Miche","Philippe","{ 'adresse': 'Route de la Gare 1', 'ville': 'Fribourg' }","0791234567");
INSERT INTO Patient (	IDPatient	,	Medecin	,	Appareil	,	Nom	,	Prenom	,	Adresse	,	NumTel	) 	VALUES(	3	,	2	,	2	,"Pauchard","Lucas","{ 'adresse': 'Rue de la Patience 3', 'ville': 'Romont' }","0797895612");

INSERT INTO Technicien (	IDTechnicien	,	Nom	,	Prenom	,	Adresse	,	NumTel	,	Email	,	Password	) 	VALUES(	1	,"Alvarez","David","{ 'adresse': 'Chemin Biseau 4', 'ville': 'Lancy' }","0796665544","tech1@test.com","e1078c2b0563bb2b12dee85859da83c2");
INSERT INTO Technicien (	IDTechnicien	,	Nom	,	Prenom	,	Adresse	,	NumTel	,	Email	,	Password	) 	VALUES(	2	,"Dumas","Ayrton","{ 'adresse': 'Route de la Strasse 15', 'ville': 'Fribourg' }","0798883355","tech2@test.com","e1078c2b0563bb2b12dee85859da83c2");
INSERT INTO Technicien (	IDTechnicien	,	Nom	,	Prenom	,	Adresse	,	NumTel	,	Email	,	Password	) 	VALUES(	3	,"Mauron","Jocelyn","{ 'adresse': 'Route de la Strasse 5', 'ville': 'Fribourg' }","0796151341","tech3@test.com","e1078c2b0563bb2b12dee85859da83c2");

INSERT INTO ResponsableTechnique (	IDRespTechnique	,	Nom	,	Prenom	,	Adresse	,	NumTel	,	Email	,	Password	) 	VALUES(	1	,"Ingram","Sandy","{ 'adresse': 'Chemin des Profs 45', 'ville': 'Neyruz' }","0794459334","resp1@test.com","e1078c2b0563bb2b12dee85859da83c2");
INSERT INTO ResponsableTechnique (	IDRespTechnique	,	Nom	,	Prenom	,	Adresse	,	NumTel	,	Email	,	Password	) 	VALUES(	2	,"Abou Khaled","Omar","{ 'adresse': 'Chemin des Profs 35', 'ville': 'Neyruz' }","0785542345","resp2@test.com","e1078c2b0563bb2b12dee85859da83c2");
INSERT INTO ResponsableTechnique (	IDRespTechnique	,	Nom	,	Prenom	,	Adresse	,	NumTel	,	Email	,	Password	) 	VALUES(	3	,"Bruegger","Pascal","{ 'adresse': 'Chemin des Profs 43', 'ville': 'Neyruz' }","0786543265","resp3@test.com","e1078c2b0563bb2b12dee85859da83c2");

INSERT INTO Visiter ( Technicien,	Patient	,	Date	,	Confirme	) 		VALUES(	1	,	1	,"2020-01-12",	0	);
INSERT INTO Visiter ( Technicien,	Patient	,	Date	,	Confirme	) 		VALUES(	2	,	2	,"2021-02-12",	0	);
INSERT INTO Visiter ( Technicien,	Patient	,	Date	,	Confirme	) 		VALUES(	3	,	3	,"2022-03-12",	0	);
INSERT INTO Visiter ( Technicien,	Patient	,	Date	,	Confirme	) 		VALUES(	1	,	2	,"2023-01-12",	0	);
INSERT INTO Visiter ( Technicien,	Patient	,	Date	,	Confirme	) 		VALUES(	2	,	2	,"2024-02-12",	0	);
INSERT INTO Visiter ( Technicien,	Patient	,	Date	,	Confirme	) 		VALUES(	3	,	3	,"2025-03-12",	0	);
INSERT INTO Visiter ( Technicien,	Patient	,	Date	,	Confirme	) 		VALUES(	1	,	3	,"2026-01-12",	0	);
INSERT INTO Visiter ( Technicien,	Patient	,	Date	,	Confirme	) 		VALUES(	2	,	2	,"2027-02-12",	0	);
INSERT INTO Visiter ( Technicien,	Patient	,	Date	,	Confirme	) 		VALUES(	3	,	3	,"2028-03-12",	0	);