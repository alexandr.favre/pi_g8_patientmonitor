
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`ResponsableTechnique`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`ResponsableTechnique` ;

CREATE TABLE IF NOT EXISTS `mydb`.`ResponsableTechnique` (
  `IDRespTechnique` INT NOT NULL,
  `Nom` VARCHAR(45) NOT NULL,
  `Prenom` VARCHAR(45) NOT NULL,
  `Adresse` JSON NOT NULL,
  `NumTel` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NOT NULL,
  `HashPersonne` VARCHAR(64) NULL,
  PRIMARY KEY (`IDRespTechnique`),
  UNIQUE INDEX `User` (`Email` ASC)  ,
  UNIQUE INDEX `HashPersonne_UNIQUE` (`HashPersonne` ASC)  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Technicien`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Technicien` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Technicien` (
  `IDTechnicien` INT NOT NULL,
  `Nom` VARCHAR(45) NOT NULL,
  `Prenom` VARCHAR(45) NOT NULL,
  `Adresse` JSON NOT NULL,
  `NumTel` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `Password` VARCHAR(45) NOT NULL,
  `HashPersonne` VARCHAR(64) NULL,
  PRIMARY KEY (`IDTechnicien`),
  UNIQUE INDEX `User` (`Email` ASC)  ,
  UNIQUE INDEX `HashPersonne_UNIQUE` (`HashPersonne` ASC)  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Medecin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Medecin` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Medecin` (
  `IDMedecin` INT NOT NULL,
  `Nom` VARCHAR(45) NOT NULL,
  `Prenom` VARCHAR(45) NOT NULL,
  `Adresse` JSON NOT NULL,
  `NumTel` VARCHAR(45) NOT NULL,
  `HashPersonne` VARCHAR(64) NULL,
  PRIMARY KEY (`IDMedecin`),
  UNIQUE INDEX `HashPersonne_UNIQUE` (`HashPersonne` ASC)  )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Appareil`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Appareil` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Appareil` (
  `NumAppareil` INT NOT NULL,
  `Type` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`NumAppareil`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Patient`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Patient` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Patient` (
  `IDPatient` INT NOT NULL,
  `Medecin` INT NOT NULL,
  `Appareil` INT NOT NULL,
  `Nom` VARCHAR(45) NOT NULL,
  `Prenom` VARCHAR(45) NOT NULL,
  `Adresse` JSON NOT NULL,
  `NumTel` VARCHAR(45) NOT NULL,
  `HashPersonne` VARCHAR(64) NULL,
  `Timestamp` INTEGER NULL DEFAULT 0,
  INDEX `Medecin_idx` (`Medecin` ASC)  ,
  INDEX `Appareil_idx` (`Appareil` ASC)  ,
  PRIMARY KEY (`IDPatient`),
  UNIQUE INDEX `HashPersonne_UNIQUE` (`HashPersonne` ASC)  ,
  CONSTRAINT `Medecin`
    FOREIGN KEY (`Medecin`)
    REFERENCES `mydb`.`Medecin` (`IDMedecin`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Appareil`
    FOREIGN KEY (`Appareil`)
    REFERENCES `mydb`.`Appareil` (`NumAppareil`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Visiter`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`Visiter` ;

CREATE TABLE IF NOT EXISTS `mydb`.`Visiter` (
  `Technicien` INT NOT NULL,
  `Patient` INT NOT NULL,
  `Date` DATE NOT NULL,
  `Confirme` BOOLEAN NOT NULL,
  `Timestamp` INTEGER NULL DEFAULT 0,
  INDEX `Technicien_idx` (`Technicien` ASC)  ,
  INDEX `Patient_idx` (`Patient` ASC)  ,
  PRIMARY KEY (`Date`, `Patient`),
  CONSTRAINT `Technicien`
    FOREIGN KEY (`Technicien`)
    REFERENCES `mydb`.`Technicien` (`IDTechnicien`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Patient`
    FOREIGN KEY (`Patient`)
    REFERENCES `mydb`.`Patient` (`IDPatient`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`PhotoVisiter`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mydb`.`PhotoVisiter` ;

CREATE TABLE IF NOT EXISTS `mydb`.`PhotoVisiter` (
  `Patient` INT NOT NULL,
  `Date` DATE NOT NULL,
  `IDPhoto` INT NOT NULL AUTO_INCREMENT,
  `URLPhoto` MEDIUMTEXT NOT NULL,
  INDEX `Visiter_idx` (`Patient` ASC, `Date` ASC)  ,
  PRIMARY KEY (`IDPhoto`),
  CONSTRAINT `Visiter`
    FOREIGN KEY (`Patient` , `Date`)
    REFERENCES `mydb`.`Visiter` (`Patient` , `Date`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


USE `mydb`;

DELIMITER $$

USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`ResponsableTechnique_BEFORE_INSERT` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`ResponsableTechnique_BEFORE_INSERT` BEFORE INSERT ON `ResponsableTechnique` FOR EACH ROW
BEGIN
DECLARE newHashPersonne varchar(64);

    Select sha2(CONCAT(new.Nom,new.Prenom,new.NumTel,new.Adresse),0) into @newHashPersonne;

	IF EXISTS(
		SELECT HashPersonne FROM(
			SELECT HashPersonne From Patient
            UNION ALL
            SELECT HashPersonne From Technicien
            UNION ALL
            SELECT HashPersonne From Medecin
		) AS Personne
        WHERE Personne.HashPersonne LIKE @newHashPersonne
	)
    THEN
		signal sqlstate '45000' set message_text = 'This Person already exists';
	END IF;

    Set new.HashPersonne = @newHashPersonne;

	IF EXISTS(
		SELECT Email From Technicien
		WHERE Technicien.Email=new.Email
	)
	THEN
		signal sqlstate '45000' set message_text = 'This User already exists';
	END IF;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`ResponsableTechnique_BEFORE_UPDATE` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`ResponsableTechnique_BEFORE_UPDATE` BEFORE UPDATE ON `ResponsableTechnique` FOR EACH ROW
BEGIN
	DECLARE newHashPersonne varchar(64);

    Select sha2(CONCAT(new.Nom,new.Prenom,new.NumTel,new.Adresse),0) into @newHashPersonne;

	IF EXISTS(
		SELECT HashPersonne FROM(
			SELECT HashPersonne From Patient
            UNION ALL
            SELECT HashPersonne From Technicien
            UNION ALL
            SELECT HashPersonne From Medecin
		) AS Personne
        WHERE Personne.HashPersonne LIKE @newHashPersonne
	)
    THEN
		signal sqlstate '45000' set message_text = 'This Person already exists';
	END IF;

    Set new.HashPersonne = @newHashPersonne;

	IF EXISTS(
		SELECT Email From Technicien
		WHERE Technicien.Email=new.Email
	)
	THEN
		signal sqlstate '45000' set message_text = 'This User already exists';
	END IF;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`Technicien_BEFORE_INSERT` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`Technicien_BEFORE_INSERT` BEFORE INSERT ON `Technicien` FOR EACH ROW
BEGIN
DECLARE newHashPersonne varchar(64);

    Select sha2(CONCAT(new.Nom,new.Prenom,new.NumTel,new.Adresse),0) into @newHashPersonne;

	IF EXISTS(
		SELECT HashPersonne FROM(
			SELECT HashPersonne From ResponsableTechnique
            UNION ALL
            SELECT HashPersonne From Patient
            UNION ALL
            SELECT HashPersonne From Medecin
		) AS Personne
        WHERE Personne.HashPersonne LIKE @newHashPersonne
	)
    THEN
		signal sqlstate '45000' set message_text = 'This Person already exists';
	END IF;

    Set new.HashPersonne = @newHashPersonne;

	IF EXISTS(
		SELECT Email From ResponsableTechnique
		WHERE ResponsableTechnique.Email=new.Email
	)
	THEN
		signal sqlstate '45000' set message_text = 'This User already exists';
	END IF;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`Medecin_BEFORE_INSERT` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`Medecin_BEFORE_INSERT` BEFORE INSERT ON `Medecin` FOR EACH ROW
BEGIN
DECLARE newHashPersonne varchar(64);

    Select sha2(CONCAT(new.Nom,new.Prenom,new.NumTel,new.Adresse),0) into @newHashPersonne;

	IF EXISTS(
		SELECT HashPersonne FROM(
			SELECT HashPersonne From ResponsableTechnique
            UNION ALL
            SELECT HashPersonne From Technicien
            UNION ALL
            SELECT HashPersonne From Patient
		) AS Personne
        WHERE Personne.HashPersonne LIKE @newHashPersonne
	)
    THEN
		signal sqlstate '45000' set message_text = 'This Person already exists';
	END IF;

    Set new.HashPersonne = @newHashPersonne;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`Medecin_BEFORE_UPDATE` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`Medecin_BEFORE_UPDATE` BEFORE UPDATE ON `Medecin` FOR EACH ROW
BEGIN
DECLARE newHashPersonne varchar(64);

 Select sha2(CONCAT(new.Nom,new.Prenom,new.NumTel,new.Adresse),0) into @newHashPersonne;

	IF EXISTS(
		SELECT HashPersonne FROM(
			SELECT HashPersonne From ResponsableTechnique
            UNION ALL
            SELECT HashPersonne From Technicien
            UNION ALL
            SELECT HashPersonne From Patient
		) AS Personne
        WHERE Personne.HashPersonne LIKE @newHashPersonne
	)
    THEN
		signal sqlstate '45000' set message_text = 'This Person already exists';
	END IF;

    Set new.HashPersonne = @newHashPersonne;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`Appareil_BEFORE_INSERT` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`Appareil_BEFORE_INSERT` BEFORE INSERT ON `Appareil` FOR EACH ROW
BEGIN
	IF new.Type<>'S8' AND new.Type<>'S9' AND new.Type<>'S10' THEN
		signal sqlstate '45000' set message_text = 'Appareil type must be S8,S9 or S10';
    END IF;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`Appareil_BEFORE_UPDATE` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`Appareil_BEFORE_UPDATE` BEFORE UPDATE ON `Appareil` FOR EACH ROW
BEGIN
	IF new.Type<>'S8' AND new.Type<>'S9' AND new.Type<>'S10' THEN
		signal sqlstate '45000' set message_text = 'Appareil type must be S8,S9 or S10';
    END IF;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`Patient_BEFORE_INSERT` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`Patient_BEFORE_INSERT` BEFORE INSERT ON `Patient` FOR EACH ROW
BEGIN
	DECLARE newHashPersonne varchar(64);

    Select sha2(CONCAT(new.Nom,new.Prenom,new.NumTel,new.Adresse),0) into @newHashPersonne;

	IF EXISTS(
		SELECT HashPersonne FROM(
			SELECT HashPersonne From ResponsableTechnique
            UNION ALL
            SELECT HashPersonne From Technicien
            UNION ALL
            SELECT HashPersonne From Medecin
		) AS Personne
        WHERE Personne.HashPersonne LIKE @newHashPersonne
	)
    THEN
		signal sqlstate '45000' set message_text = 'This Person already exists';
	END IF;

    Set new.HashPersonne = @newHashPersonne;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`Patient_BEFORE_UPDATE` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`Patient_BEFORE_UPDATE` BEFORE UPDATE ON `Patient` FOR EACH ROW
BEGIN
DECLARE newHashPersonne varchar(64);

    Select sha2(CONCAT(new.Nom,new.Prenom,new.NumTel,new.Adresse),0) into @newHashPersonne;

	IF EXISTS(
		SELECT HashPersonne FROM(
			SELECT HashPersonne From ResponsableTechnique
            UNION ALL
            SELECT HashPersonne From Technicien
            UNION ALL
            SELECT HashPersonne From Medecin
		) AS Personne
        WHERE Personne.HashPersonne LIKE @newHashPersonne
	)
    THEN
		signal sqlstate '45000' set message_text = 'This Person already exists';
	END IF;

	Set new.timestamp = old.timestamp + 1;
    Set new.HashPersonne = @newHashPersonne;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`Technicien_BEFORE_INSERT` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`Technicien_BEFORE_INSERT` BEFORE INSERT ON `Technicien` FOR EACH ROW
BEGIN
DECLARE newHashPersonne varchar(64);

    Select sha2(CONCAT(new.Nom,new.Prenom,new.NumTel,new.Adresse),0) into @newHashPersonne;

	IF EXISTS(
		SELECT HashPersonne FROM(
			SELECT HashPersonne From ResponsableTechnique
            UNION ALL
            SELECT HashPersonne From Patient
            UNION ALL
            SELECT HashPersonne From Medecin
		) AS Personne
        WHERE Personne.HashPersonne LIKE @newHashPersonne
	)
    THEN
		signal sqlstate '45000' set message_text = 'This Person already exists';
	END IF;

    Set new.HashPersonne = @newHashPersonne;

	IF EXISTS(
		SELECT Email From ResponsableTechnique
		WHERE ResponsableTechnique.Email=new.Email
	)
	THEN
		signal sqlstate '45000' set message_text = 'This User already exists';
	END IF;
END$$


USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`Technicien_BEFORE_UPDATE` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`Technicien_BEFORE_UPDATE` BEFORE UPDATE ON `Technicien` FOR EACH ROW
BEGIN
DECLARE newHashPersonne varchar(64);

    Select sha2(CONCAT(new.Nom,new.Prenom,new.NumTel,new.Adresse),0) into @newHashPersonne;

	IF EXISTS(
		SELECT HashPersonne FROM(
			SELECT HashPersonne From ResponsableTechnique
            UNION ALL
            SELECT HashPersonne From Patient
            UNION ALL
            SELECT HashPersonne From Medecin
		) AS Personne
        WHERE Personne.HashPersonne LIKE @newHashPersonne
	)
    THEN
		signal sqlstate '45000' set message_text = 'This Person already exists';
	END IF;

    Set new.HashPersonne = @newHashPersonne;

	IF EXISTS(
		SELECT Email From ResponsableTechnique
		WHERE ResponsableTechnique.Email=new.Email
	)
	THEN
		signal sqlstate '45000' set message_text = 'This User already exists';
	END IF;
END$$

USE `mydb`$$
DROP TRIGGER IF EXISTS `mydb`.`VISITER_BEFORE_UPDATE` $$
USE `mydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `mydb`.`VISITER_BEFORE_UPDATE` BEFORE UPDATE ON `Visiter` FOR EACH ROW
BEGIN
    Set new.timestamp = old.timestamp+1;
END$$


DELIMITER ;

CREATE OR REPLACE USER
'adminmydb' IDENTIFIED BY 'mypassword';

GRANT ALL PRIVILEGES ON mydb.*
TO 'adminmydb';

CREATE OR REPLACE USER
'webmydb' IDENTIFIED BY 'mypassword';

GRANT SELECT, INSERT, UPDATE, DELETE ON mydb.*
TO 'webmydb';