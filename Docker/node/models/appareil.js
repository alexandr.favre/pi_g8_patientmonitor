
const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define("Appareil", {
    NumAppareil: {
      type: Sequelize.INTEGER,
	  autoIncrement: true,
      primaryKey: true,
    },
    Type: {
      type: Sequelize.STRING,
    }
  },{
    freezeTableName: true,
    timestamps: false
  });
}
