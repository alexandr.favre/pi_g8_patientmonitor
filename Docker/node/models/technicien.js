
const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define("Technicien", {
    IDTechnicien: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    Nom: {
      type: Sequelize.STRING,
    },
    Prenom: {
      type: Sequelize.STRING,
    },
    Adresse: {
      type: Sequelize.STRING,
    },
    NumTel: {
      type: Sequelize.STRING,
    },
    Email: {
      type: Sequelize.STRING,
    },
    Password: {
      type: Sequelize.STRING,
    },
    HashPersonne: {
      type: Sequelize.STRING,
    }
  },{
    freezeTableName: true,
    timestamps: false
  });
}
