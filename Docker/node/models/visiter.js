
const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const Visiter = sequelize.define("Visiter", {
    Technicien: {
      type: Sequelize.INTEGER,
    },
    Patient: {
      type: Sequelize.INTEGER,
    },
    Date: {
      type: Sequelize.DATE,
    },
    Confirme: {
      type: Sequelize.BOOLEAN,
    },
	Timestamp: {
	  type: Sequelize.INTEGER,
	}
  },{
    freezeTableName: true,
    timestamps: false
  });
  Visiter.removeAttribute('id');
  return Visiter;
}
