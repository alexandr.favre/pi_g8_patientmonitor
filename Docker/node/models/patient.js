
const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define("Patient", {
    IDPatient: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    Nom: {
      type: Sequelize.STRING,
    },
    Prenom: {
      type: Sequelize.STRING,
    },
    Adresse: {
      type: Sequelize.STRING,
    },
    NumTel: {
      type: Sequelize.STRING,
    },
	Medecin: {
	  type: Sequelize.INTEGER,
	},
	Appareil: {
	  type: Sequelize.INTEGER,
	},
    HashPersonne: {
      type: Sequelize.STRING,
    },
	Timestamp: {
	  type: Sequelize.INTEGER,
	}
  },{
    freezeTableName: true,
    timestamps: false
  });
}
