
const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const PhotoVisiter = sequelize.define("PhotoVisiter", {
    Patient: {
      type: Sequelize.INTEGER,
    },
    Date: {
      type: Sequelize.DATE,
    },
    IDPhoto: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    URLPhoto: {
      type: Sequelize.STRING,
    }
  }, {
      freezeTableName: true,
      timestamps: false
    });
  return PhotoVisiter;
}
