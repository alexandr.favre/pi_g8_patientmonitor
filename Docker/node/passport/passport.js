
module.exports = function (sequelize) {

    const passport = require('passport')
    const passportJWT = require("passport-jwt")

    const ExtractJWT = passportJWT.ExtractJwt

    const LocalStrategy = require('passport-local').Strategy
    const JWTStrategy = passportJWT.Strategy

    const Technicien = sequelize.import('../models/technicien')
    const ResponsableTechnique = sequelize.import('../models/responsabletechnique')

    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
        function (email, password, cb) {
            sequelize.authenticate().then(() => {
                return ResponsableTechnique.findOne({
                    where: {
                        email: email,
                        password: password
                    }
                }).then(respTech => {
                    if (respTech != null) {
                        identifierValue = "2"
                        return cb(null, respTech, identifierValue)
                    } else {
                        return Technicien.findOne({
                            where: {
                                email: email,
                                password: password
                            }
                        }).then(technician => {
                            identifierValue = ((technician == null) ? "0" : "1")
                            return cb(null, technician, identifierValue)
                        }).catch(err => {
                            return cb(err)
                        })
                    }
                }).catch(err => {
                    return cb(err)
                })
            })
        }
    ))

    passport.use(new JWTStrategy({
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: 'michepauchardfavre8'
    },
        function (jwtPayload, cb) {

            console.log(jwtPayload)

            return ResponsableTechnique.findOne({
                where: {
                    IDRespTechnique: jwtPayload.IDRespTechnique
                }
            }).then(user => {
                return cb(null, user);
            })
                .catch(err => {
                    return Technicien.findOne({
                        where: {
                            IDTechnicien: jwtPayload.IDTechnicien
                        }
                    }).then(user => {
                        return cb(null, user);
                    })
                        .catch(err => {
                            return cb(err);
                        });
                    return cb(err);
                });
        }
    ));
}