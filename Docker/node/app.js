
var express = require('express');
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.json());

var path = require('path');

app.use(express.static('public'));
app.use('/img', express.static(__dirname + '/img'));

const Sequelize = require('sequelize');
const sequelize = new Sequelize({
  database: 'mydb',
  username: 'webmydb',
  password: 'mypassword',
  host: '172.17.0.1', // Docker : 172.17.0.1 | Toolbox : 192.168.99.100
  port: 3306,
  dialect: 'mariadb'
});

require('./routes')(app, sequelize);
require('./passport/passport')(sequelize);

app.listen(8080, function() {
  console.log("App Started on PORT 8080");
});
