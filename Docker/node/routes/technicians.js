
module.exports = function(app, sequelize){

	const passport = require('passport')

	const Technicien = sequelize.import('../models/technicien')

	// Get all technicians
	app.get('/technicians', passport.authenticate('jwt', {session: false}), (req, res) => {
		Technicien.findAll({
			attributes: ['IDTechnicien', 'Nom', 'Prenom', 'Adresse', 'NumTel']
		}).then(technicians => res.json(technicians))
	})

	// Update a technician
	app.post('/update_technician', passport.authenticate('jwt', {session: false}), function(req,res){
		Technicien.update(
			{Nom:req.body.Nom, Prenom:req.body.Prenom, Adresse:req.body.Adresse, NumTel:req.body.NumTel},
			{where:{IDTechnicien:req.body.IDTechnicien}}
		).then(success=> res.json(true))
	})
}
