
module.exports = function (app, sequelize) {

	const multer = require('multer')
	const passport = require('passport')

	var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, __dirname + '/../img/')
		},
		filename: function (req, file, cb) {
			cb(null, 'img_' + Date.now() + '.jpg')
		}
	})

	var upload = multer({ storage: storage })

	var fs = require('fs');

	const Technicien = sequelize.import('../models/technicien')
	const Medecin = sequelize.import('../models/medecin')
	const Patient = sequelize.import('../models/patient')
	const Visiter = sequelize.import('../models/visiter')
	const PhotoVisiter = sequelize.import('../models/photovisiter')
	const Appareil = sequelize.import('../models/appareil')

	app.post('/sync', passport.authenticate('jwt', { session: false }), function (req, res) {
		var email = req.body.email
		sequelize.authenticate().then(() => {
			Technicien.findOne({
				where: {
					email: email
				}
			}).then(async technician => {

				let operations = req.body.operations;
				for (var i = 0; i < operations.length; i++) {

					let t;
					try {
						t = await sequelize.transaction({ isolationLevel: "SERIALIZABLE", autocommit: false })
						if (operations[i].Table === "Visiter") {
							//UPDATE VISITE
							var oldVisit = await Visiter.findOne({
								where: {
									Patient: operations[i].P1,
									Date: operations[i].P2
								}, transaction: t
							})
							if (oldVisit != null) {
								if (oldVisit.Timestamp != operations[i].Timestamp) {
									//LOG!
									var msg = 'CONFLIT: La donnée Visiter(' + oldVisit.Technicien + ',' + oldVisit.Date + ',' + oldVisit.Patient + ',' + oldVisit.Confirme + ') a été remplacé avec la requête :' + operations[i].Request;
									fs.appendFile(__dirname + '/../conflicts.log', msg, function (err) {
										if (err) {
											return trueLog(err);
										}
									});
								}
								sequelize.query(operations[i].Request, { transaction: t });
							} else {
								var msg = 'Le technicien <' + technician.Prenom + ' ' + technician.Nom + '>{id: ' + technician.IDTechnicien + '} a essayer de mettre à jour la visite {' + operations[i].P2 + ',' + operations[i].P1 + '} mais est la donnée est soit inexistante soit a été supprimée.';
								fs.appendFile(__dirname + '/../conflicts.log', msg, function (err) {
									if (err) {
										return trueLog(err);
									}
								});
							}
						} else if (operations[i].Table === "Patient") {
							//UPDATE PATIENT
							var oldPatient = await Patient.findOne({
								where: {
									IDPatient: operations[i].P1,
								}, transaction: t
							})
							if (oldPatient != null) {
								if (oldPatient.Timestamp != operations[i].Timestamp) {
									//LOG!
									var msg = 'CONFLIT: La donnée Patient(' + oldPatient.IDPatient + ',' + oldPatient.Nom + ',' + oldPatient.Prenom + ',' + oldPatient.Adresse + ',' + oldPatient.NumTel + ',' + oldPatient.Medecin + ',' + oldPatient.Appareil + ') a été remplacé avec la requête :' + operations[i].Request;
									fs.appendFile(__dirname + '/../conflicts.log', msg, function (err) {
										if (err) {
											return trueLog(err);
										}
									});
								}
								sequelize.query(operations[i].Request, { transaction: t });
							} else {
								var msg = 'Le technicien <' + technician.Prenom + ' ' + technician.Nom + '>{id: ' + technician.IDTechnicien + '} a essayer de mettre à jour la visite {' + operations[i].P2 + ',' + operations[i].P1 + '} mais est la donnée est soit inexistante soit a été supprimée.';
								fs.appendFile(__dirname + '/../conflicts.log', msg, function (err) {
									if (err) {
										return trueLog(err);
									}
								});
							}
						}
						await t.commit();
					} catch (e) {
						console.log('ERROR TRANSACTION SYNC: ' + e);
						res.send(e);
						await t.roolback();
					}
				}

				technicianID = technician.IDTechnicien
				Medecin.findAll({ attributes: ['IDMedecin', 'Nom', 'Prenom', 'Adresse', 'NumTel'] }).then(doctors => {
					Appareil.findAll().then(appareils => {
						sequelize.query('Select IDPatient,Medecin,Appareil,Nom,Prenom,Adresse,NumTel,Timestamp from Patient where Patient.IDPatient IN (Select Visiter.Patient from Visiter where Visiter.Technicien=?)',
							{ replacements: [technicianID], model: Patient }).then(patients => {
								Visiter.findAll({ attributes: ['Technicien', 'Patient', 'Date', 'Confirme', 'Timestamp'], where: { technicien: technicianID } }).then(visites => {
									res.json({
										medecin: doctors,
										appareil: appareils,
										patient: patients,
										visiter: visites
									});
								});
							});
					});
				});
			}).catch(error => console.log(error));
		});
	});

	app.post('/upload_image', passport.authenticate('jwt', { session: false }), upload.single('file'), async (req, res) => {

		var isOkay = false;

		var visit = await Visiter.findOne({
			where: {
				Patient: req.body.Patient,
				Date: req.body.Date
			}
		})
		if (visit == null) {
			//LOG!
			var msg = 'La donnée Visit(' + req.body.Patient + ',' + req.body.Date + ') est inexistante. L\'ajout de l\'image est donc impossible!'
			fs.appendFile(__dirname + '/../conflicts.log', msg, function (err) {
				if (err) {
					return trueLog(err);
				}
			});
			res.send(isOkay);
			return;
		}

		PhotoVisiter.create({
			Date: req.body.Date,
			Patient: req.body.Patient,
			URLPhoto: __dirname + '/../img/' + req.file.filename
		})

		res.send(isOkay);
	})
}
