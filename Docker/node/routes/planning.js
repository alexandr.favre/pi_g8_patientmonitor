module.exports = function (app, sequelize) {

	const passport = require('passport')

	const Patient = sequelize.import('../models/patient')
	const Visiter = sequelize.import('../models/visiter')

	//Get Planning
	app.post('/planning', passport.authenticate('jwt', { session: false }), function (req, res) {

		var id_tech = req.body.idTechnicien;
		sequelize.query('Select Technicien, Patient, Date from Visiter where Visiter.Technicien=? AND Visiter.Confirme=false',
			{ replacements: [id_tech], model: Visiter }).then(visites => {
				sequelize.query('Select IDPatient,Medecin,Appareil,Nom,Prenom,Adresse,NumTel,Timestamp from Patient where Patient.IDPatient IN (Select Visiter.Patient from Visiter where Visiter.Technicien=?)',
					{ replacements: [id_tech], model: Patient }).then(patients => {
						res.json({
							visiter: visites,
							patient: patients
						});
					});
			});
	});

	//Delete Visit
	app.post('/delete_visit', passport.authenticate('jwt', { session: false }), function (req, res) {
		Visiter.destroy({
			where: {
				Date: req.body.Date,
				Patient: req.body.Patient
			}
		}).then(success => res.json(true))
	});

	//Update Visit
	app.post('/update_visit', passport.authenticate('jwt', { session: false }), function (req, res) {
		var id_patient_before = req.body.oldVisit.Patient;
		var date_before = req.body.oldVisit.Date;

		var id_patient_after = req.body.newVisit.Patient;
		var date_after = req.body.newVisit.Date;


		Visiter.update(
			{ Date: date_after, Patient: id_patient_after },
			{
				where: {
					Date: date_before,
					Patient: id_patient_before
				}
			}
		).then(success => res.json(true))
	});

	//Add Visit
	app.post('/add_visit', passport.authenticate('jwt', { session: false }), function (req, res) {
		Visiter.create({
			Date: req.body.Date,
			Patient: req.body.Patient,
			Technicien: req.body.Technicien,
			Confirme: false

		}).then(success => res.json(true))
	});
}
