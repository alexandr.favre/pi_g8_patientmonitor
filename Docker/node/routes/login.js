
module.exports = function (app, sequelize) {

  const jwt = require('jsonwebtoken')
  const passport = require('passport')

  app.post('/login', function (req, res, next) {

    var identifierValue = "0"

    passport.authenticate('local', { session: false }, (err, user, identifierValue) => {
      if (err || identifierValue == '0') {
        return res.json({
          identifierValue: identifierValue,
          token: null
        })
      }
      const token = jwt.sign(user.toJSON(), 'michepauchardfavre8');
      return res.json({
        identifierValue: identifierValue,
        token: token
      })
    })(req, res);
  })

  app.get('/is_logged', passport.authenticate('jwt', { session: false }), function (req, res, next) {
    res.send(true)
  })
}