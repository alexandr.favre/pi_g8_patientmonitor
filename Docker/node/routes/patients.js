
module.exports = function(app, sequelize){

	const passport = require('passport')

	const Patient = sequelize.import('../models/patient')

	// Get all patients
	app.get('/patients', passport.authenticate('jwt', {session: false}), (req, res) => {
		Patient.findAll({
			attributes: ['IDPatient', 'Nom', 'Prenom']
		}).then(patients => res.json(patients))
	})
}
